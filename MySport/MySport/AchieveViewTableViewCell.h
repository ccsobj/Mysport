//
//  AchieveViewTableViewCell.h
//  MySport
//
//  Created by lanou3g on 16/2/27.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AchieveViewTableViewCell : UITableViewCell
@property (nonatomic ,strong)UILabel *rankinglabel;//排名
@property (nonatomic ,strong)UILabel *numberlabel;//步数
@property (nonatomic ,strong)UILabel *namelabel;//名字
@property (nonatomic ,strong)UIImageView *image;//头像
@property (nonatomic ,strong)UIButton *Pkbut;//pk按钮
@property (nonatomic ,strong)UIButton *attention;//关注按钮
@property (nonatomic ,strong)UILabel *sexlabel;//性别
@end
