//
//  ActivityViewController.swift
//  MySport
//
//  Created by lanou3g on 16/3/3.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

class ActivityViewController: UIViewController,UIWebViewDelegate {
    var string = NSString()
    var web = UIWebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        web.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
        web.scalesPageToFit = true
        web.userInteractionEnabled = true
        web.delegate = self
        web.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
        self.view = web
        let request = NSURLRequest(URL: NSURL(string: string as String)!)
        web.loadRequest(request)

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
