//
//  MessageTableViewController.swift
//  MySport
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

class MessageTableViewController: UITableViewController,PushableProtocol {

    var modleArray : MessageResult?
    weak var navVC : UINavigationController?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(MessageTableViewCell.self, forCellReuseIdentifier:"cell")
        
        getData()
    }
    private func getData(){
        let param = MessageParam()
        DisconverTool.getMessageWithParam(param){
            
            self.modleArray = $0
            
            self.tableView.reloadData()
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath:indexPath)
        if let arr = self.modleArray{
            (cell as! MessageTableViewCell).updteWithMessage(arr[indexPath.row])
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.modleArray?.count{
            return count
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let message = MessageViewController()
        let url = self.modleArray![indexPath.row]?.url
        message.NString = url!
        self.pushViewController(message)
        print(url)

    }

    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 200
    }

    

}











