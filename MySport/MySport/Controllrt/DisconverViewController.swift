//
//  HomeViewController.swift
//  WangYiNews
//
//  Created by lanou3g on 16/1/19.
//  Copyright © 2016年 lanou3g. All rights reserved.
//

import UIKit
import SnapKit

let segmenHeight:CGFloat = 45.0

class DisconverViewController: UIViewController,TableConposedCollectionViewDelegate{
    
    enum IndexChangeType{
        case SegmenControl
        case CollectionView
    }
    
    private let items = ["健身圈","健身资讯","健身活动"]
    
//  控制器的存储属性

//    顶部组合工具条，包含segment选择器
    private  var  topToolBar:CustomSegmentControl!

//    主体collectionView其中包含系列TableView
    private  var collectionView : TableConposedCollectionView!
    
    private var curentPage:Int = 0
    
//    视图加载完成
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
//    初始化并添加顶部工具条
        self.topToolBar = CustomSegmentControl(items:self.items){index in
            self.changeWithIndex(index, type:.SegmenControl)
        }
        self.view.addSubview(self.topToolBar)
        
//        self.navigationController
        
//    设置点击collectionView下tableView点击cell时候的动作
        self.collectionView = TableConposedCollectionView(delegate:self)
        self.view.addSubview(self.collectionView)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        constractAllViews()
    }
    
//   首页视图切换 主要业务
    func changeWithIndex(index:Int,type:IndexChangeType){
        if index == self.curentPage {return}
        self.curentPage = index
        switch type{
            case .SegmenControl:
                self.collectionView.updateWithIndex(index)
            case .CollectionView:
                self.topToolBar.updateIndex(index)
        }
    }

//    TableConposedCollectionView的代理方法
    func tableConposedCollectionView(collectionView:TableConposedCollectionView , didChangeWithIndex index:Int){
        self.changeWithIndex(index, type:.CollectionView)
    }
    
//    为所有子视图添加约束
    private func constractAllViews(){
        self.topToolBar.snp_makeConstraints(){
        $0.top.equalTo((self.navigationController?.navigationBar.snp_bottom)!)
            $0.width.equalTo(self.view.snp_width)
            $0.height.equalTo(segmenHeight)
        }
        self.collectionView.snp_makeConstraints(){
            $0.top.equalTo(self.topToolBar.snp_bottom)
            $0.bottom.equalTo((self.tabBarController?.tabBar.snp_top)!)
            $0.width.equalTo(self.view.snp_width)
        }
    }
}