//
//  CommunityTableViewController.swift
//  MySport
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

@objc class CommunityTableViewController: UITableViewController,PushableProtocol {

    var modleArray : CommunityResult?
    weak var navVC : UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier:"cell")
        self.tableView.tableHeaderView = TableHeadView()
        
        self.refresh()
    }
    
    private func refresh(){
        self.refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action:"refreshValueChange", forControlEvents: .ValueChanged)
        self.tableView.addSubview(refreshControl!)
        refreshControl?.beginRefreshing()
        self.refreshValueChange()
    }
    
    func refreshValueChange(){
        self.getData()
    }
    
    private func getData(){
        //param 就是网址  默认初始化
        let param = ComunityParam()
        //自定义   get解析的方法
        DisconverTool.getComminityWithParam(param){
            if let result = $0{
                self.modleArray = result
                self.updateHeadView([result[0]!.image_url!,result[1]!.image_url!,result[2]!.image_url!])
                self.tableView.reloadData()
                self.refreshControl?.endRefreshing()
            }
        }
    }
    
    private func updateHeadView(urls:[String]){
        if let headView = self.tableView.tableHeaderView as? TableHeadView{
            headView.updateWithImageUrls(urls)
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath:indexPath)
        if let arr = self.modleArray{
            cell.textLabel?.text = arr[indexPath.row]?.content
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.modleArray?.count{
            return count
        }
        
        return 8
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
}
