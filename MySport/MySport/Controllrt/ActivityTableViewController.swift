//
//  ActivityTableViewController.swift
//  MySport
//
//  Created by lanou3g on 16/3/2.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

class ActivityTableViewController: UITableViewController,PushableProtocol {
    var dataArray : ActivityResult?
    
    weak var navVC : UINavigationController?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(ActivityTableViewCell.self, forCellReuseIdentifier:"cell")
        
        self.refresh()
    }
    private func refresh(){
        self.refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action:"refreshValueChange", forControlEvents: .ValueChanged)
        self.tableView.addSubview(refreshControl!)
        refreshControl?.beginRefreshing()
        self.refreshValueChange()
        

    }
    func refreshValueChange(){
        self.getData()
    }

    private func getData(){
        let param = ActivityParm()
        DisconverTool.getActivityWithParam(param){
            self.dataArray = $0
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

//    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        return 0
//    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.dataArray?.count{
            return count
        }
        return 0
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let activity = ActivityViewController()
        
        //由于url有的不行所以跳转到其他页面
        if self.dataArray![indexPath.row]?.activityUrl == nil {
            let alert = UIAlertController(title: "提示", message: "报名已截至", preferredStyle: UIAlertControllerStyle.Alert)
            
            let cancle = UIAlertAction(title: "返回", style: UIAlertActionStyle.Cancel, handler: { (ab) -> Void in
                
            })
            alert.addAction(cancle)
            self.presentViewController(alert, animated: true, completion:nil)
            
        }else{
            let url = self.dataArray![indexPath.row]?.activityUrl
            activity.string = url!
            self.pushViewController(activity)
            
            
        }
    }
  
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath:indexPath)
        if let arr = self.dataArray{
            (cell as! ActivityTableViewCell).updteWithActivity(arr[indexPath.row])
            
        }


        return cell
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 200
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
