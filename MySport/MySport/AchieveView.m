//
//  AchieveView.m
//  MySport
//
//  Created by lanou3g on 16/2/27.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "AchieveView.h"

@implementation AchieveView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addviews];
    }return self;
}
-(void)addviews{
    self.backgroundColor = [UIColor cyanColor];
    //创建
    self.tableview = [[UITableView alloc] initWithFrame:(self.bounds) style:(UITableViewStyleGrouped)];
    self.tableview.backgroundColor = [UIColor orangeColor];
    //分割线样式
//    self.tableview.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
//    self.tableview.separatorColor = [UIColor blackColor];
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;

    //设置行高
    [self addSubview:self.tableview];
    
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
