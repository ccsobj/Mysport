//
//  ActivityResult.swift
//  MySport
//
//  Created by lanou3g on 16/3/1.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

class ActivityResult: NSObject,ResultProtocol {
    typealias ModleClass = ActivityModel
    var modleArray : [ModleClass]!
    override init() {
        super.init()
    }
    required init(dictionaryArray:[[String:AnyObject]]){
        modleArray = [ModleClass]()
        for  dict in dictionaryArray{
            let obj = ModleClass()
            obj.setValuesForKeysWithDictionary(dict)
            modleArray.append(obj)
        }
    }
}
