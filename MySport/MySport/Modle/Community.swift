//
//  Community.swift
//  MySport
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

class Community:NSObject{
// 该条状态的id 2685717
    var id : Int = 0
//  用户的id 1040213
    var userId : Int = 0
// "89D06126-FC26-4347-AFB8-E06BA924434C"
    var uuid : String?
//     ft-user/E53805A9-9E80-406E-AA73-1287E845A1D6.jpg
    var image : String?
    
//    750X750
    var imageDesc : String?
    
//    
    var content : String?
    
//    1456720343000
    var createTime : Int = 0
    
//    评论的数目
    var commentCount : Int = 0
//    赞的数目
    var praiseCount : Int = 0
    
//    用户坐标
//    31.22979
    var lat : Float = 0.0
//    121.4104
    var lon : Float = 0.0
    
//    "310112"
    var adcode : String?
    
//  计算图片的真实url
    var image_url : String?{
        if let str = self.image{
            let range = str.rangeOfString("/")
            let range1 = Range(start:range!.endIndex, end:str.endIndex)
            let url = str.substringWithRange(range1)
            return "http://ft-user.fit-time.cn/" + url
        }
        return nil
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
    }
    
}
