//
//  ActivityModel.h
//  MySport
//
//  Created by lanou3g on 16/3/1.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityModel : NSObject
@property (nonatomic ,strong)NSString *headName;//标题
@property (nonatomic ,strong)NSString *headImage;//图片地址
@property (nonatomic ,strong)NSNumber *joinNum;//参与人数
@property (nonatomic ,strong)NSString *startTime;//开始时间
@property (nonatomic ,strong)NSString *activityUrl;//详情地址

@end
