//
//  CommunityResult.swift
//  MySport
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

@objc class CommunityResult:NSObject,ResultProtocol{
    typealias ModleClass = Community
    var modleArray : [ModleClass]!
    override required init() {
        super.init()
    }
    required init(dictionaryArray:[[String:AnyObject]]){
        modleArray = [ModleClass]()
        for  dict in dictionaryArray{
            let obj = ModleClass()
            obj.setValuesForKeysWithDictionary(dict)
            modleArray!.append(obj)
        }
    }
}
