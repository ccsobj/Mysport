//
//  MessageResult.swift
//  MySport
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

@objc class MessageResult:NSObject,ResultProtocol {
    typealias ModleClass = MessageModel
    var modleArray : [ModleClass]!
    override init() {
        super.init()
    }
    required init(dictionaryArray:[[String:AnyObject]]){
        modleArray = [ModleClass]()
        for  dict in dictionaryArray{
            let obj = ModleClass()
            obj.setValuesForKeysWithDictionary(dict)
            modleArray.append(obj)
        }
    }
}
