//
//  MessageParam.swift
//  MySport
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

class MessageParam:NSObject, ParamProtocol{
    @objc var URLString:String {
        return "http://api.fit-time.cn/ftinfo/getInfoByIds?stat=1&id=440&id=439&id=438&id=436&id=435&token=hedpk78qtzL9mtY9NMdpr8jRAIKKzn9eUW71BT7ugcdsrFTIr%2BerBw%3D%3D&device_id=F9AACD9C-F5FF-4320-B162-2ED01B544B2C&client=ios&idfa=C035B1B9-CECD-48CE-BCCF-41E2877D853A&lat=40.03049915124289&lon=116.3434795770147&adcode=110108&os=iPhone%20OS%209.2.1&model=iPhone8%2C1&ver=2.5.1"
    }
    var paramDic:[String:String]? {
        return nil
    }
    
    //    同过数据请求返回json（大字典或数组）后，将其转换为包含（可转化为模型的）字典的数组
    func dictionaryArray(json:AnyObject) ->[[String:AnyObject]]?{
        if let obj = json as? [String:AnyObject]{
            let arr = json["infos"]
            return (arr as? [[String:AnyObject]])
        }
        return nil
    }
    
}
