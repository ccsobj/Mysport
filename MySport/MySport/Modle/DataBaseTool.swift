//
//  DataBaseTool.swift
//  MySport
//
//  Created by lanou3g on 16/3/1.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

import FMDB

class DataBaseTool: NSObject {
    
    private let community = ClassMessage(name:NSStringFromClass(Community.self).removeNamespace(), ints:["createTime","commentCount","praiseCount"], texts:["userId","image","content","adcode"], doubles:["lat","lon"],uniqueText:["uuid"])
    
    private let message  = ClassMessage(name:NSStringFromClass(MessageModel.self).removeNamespace(), ints:nil, texts: ["praiseCount","photo","url","startTime"], doubles:nil,uniqueText:["title"])
    private let activity = ClassMessage(name:NSStringFromClass(ActivityModel.self).removeNamespace(), ints:nil, texts: ["headImage","joinNum","startTime","activityUrl"], doubles:nil,uniqueText:["headName"])
    
    private let db : FMDatabase
    
//    单例及初始化方法
    static let shareInstance = DataBaseTool()
    private override init() {
        let doc = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).last
        let path = doc! + "/MySport.sqlite"
        print(path)
        db = FMDatabase(path:path)
        super.init()
        if db.open(){
            do{
                try db.executeUpdate(self.sql_create(self.message), values:nil)
                try db.executeUpdate(self.sql_create(self.community), values:nil)
                try db.executeUpdate(self.sql_create(self.activity), values:nil)
                let sql = self.sql_create(self.community)
                print(sql)
            }catch let error as NSError{
                print( "创表失败\(error)")
            }
        }else{
            print("打开失败")
        }
    }
    
//  保存模型对象数组
   func saveCommunity(communitys:[Community]){
        for com in communitys {
            self.saveInstance(com, classMessage:self.community)
        }
    }
    
   func saveActivity(activity:[ActivityModel]){
        for act in activity{
            self.saveInstance(act, classMessage:self.activity)
        }
    }
    
   func saveMessage(message:[MessageModel]){
        for mes in message{
            self.saveInstance(mes, classMessage:self.message)
        }
    }
    
//    查询数据模型
    func selectActivity()->[ActivityModel]?{
       return select(self.activity) as? [ActivityModel]
    }
    
    func selectCommunity()->[Community]?{
        
       return select(self.community) as? [Community]
    }
    
    func selectMessge()->[MessageModel]?{
       return select(self.message) as? [MessageModel]
    }
    
    private func select(var msg:ClassMessage)->[NSObject]?{
        do{
            let sql = "SELECT * FROM " + msg.name
          let result = try db.executeQuery(sql, values:nil)
            var objs = [NSObject]()
            while(result.next()){
                let obj = msg.instance()
                for key in msg.keys(){
                  let value =  result.objectForColumnName(key)
                      obj?.setValue(value, forKey:key)
                }
                objs.append(obj!)
            }
            if objs.count > 0 {
                return objs
            }
        }catch let err as NSError{
            print(err)
        }
        return nil
    }
    
    private func saveInstance(instance:NSObject,var classMessage:ClassMessage){
        let name = classMessage.name
        let sql = "insert into " + name.removeNamespace() + classMessage.list()
        do{
            let keys = classMessage.keys()
            try db.executeUpdate(sql, values:self.valusForKeys(instance, keys:keys))
            
        }catch let error as NSError{
            print(error)
        }
    }
    
    private func valusForKeys(instent:NSObject,keys:[String])->[AnyObject]{
        var valus = [AnyObject]()
        for k in keys{
            
            if let obj = instent.valueForKey(k){
                      valus.append(obj)
            }
        }
        return valus
    }
}

extension DataBaseTool{
    
    enum DataType : String{
        case Int = " Integer NOT NULL,"
        case String = " Text  NOT NULL,"
        case Double = " Double NOT NULL,"
        case UniqueText = " Text  NOT NULL UNIQUE,  "
    }
    
    private func sql_create(m: ClassMessage)->String{
        
        let doub = m.doubles
        
        let sql = sql_create(m.name.removeNamespace(),unin:m.uniqueText, intpro:m.ints, textpro:m.texts, doublepro:doub)
        
        return  sql
    }
    
    private func sql_create(name:String,
        unin:[String]?=nil,
        intpro:[String]? = nil ,
        textpro:[String]? = nil,
        doublepro:[String]? = nil)->String{
        var sql = "CREATE TABLE IF NOT EXISTS " + name + "(id integer primary key autoincrement,"
            
        if let pro0 = unin{
            sql += sql_type_property(pro0, type:.UniqueText)
        }
        if let pros1 = intpro{
            sql += sql_type_property(pros1, type:.Int)
        }
        if let pros2 = textpro{
            sql += sql_type_property(pros2, type:.String)
        }
        if let pros3 = doublepro{
            sql += sql_type_property(pros3, type:.Double)
        }
        
        return  sql.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString:",")) + ");"
    }
    
    private func sql_create(name:String,tuples:[(String,DataType)])->String{
        let base = "CREATE TABLE IF NOT EXISTS "
        return  base + name + "(id integer primary key autoincrement," + sql_property(tuples) + ");"
    }
    
    private func sql_type_property(propertys:[String],type:DataType)->String{
        var tuples = [(String,DataType)]()
        for property in propertys{
            tuples.append((property,type))
        }
        return sql_property(tuples)
    }
    
    private func sql_property(tuples:[(String,DataType)])->String{
        var sqls = [String]()
        for (str,type) in tuples{
            sqls.append(str + type.rawValue)
        }
        return sqls.reduce("", combine: { $0 + " "+$1})
    }
}

struct ClassMessage {
    //        类名
    let name : String
    //      int类型的所有属性
    var ints : [String]?
    //      string类型的所有属性
    var texts : [String]?
    //        double类型的所有属性
    var doubles : [String]?
    
    var uniqueText: [String]?
    

    
    func nameWithClass(){
        
    }
    
    
    mutating func list()->String{
        let ks = keys()
        let str  = ks.reduce(""){$0 + "," + $1}
        let str1 = "(" + str.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString:",")) + ")"
        var str2 = " values ("
        for  _ in 0..<ks.count{
            str2 += "?,"
        }
        str2 = str2.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString:","))
        return str1 + str2 + ");"
    }
    
    mutating func keys()->[String]{
        var list = [String]()
        if let uni = uniqueText{
            list += uni
        }
        if let int = ints{
            list += int
        }
        if let text = texts{
            list += text
        }
        if let double = doubles{
            list += double
        }
        return list
    }
    
    mutating func instance()->NSObject?{
        if let clazz = NSClassFromString(name) as? NSObject.Type{
            return clazz.init()
        }else if let clazz = NSClassFromString(name.addNamespace()) as? NSObject.Type{
            return clazz.init()
        }
        return nil
    }
}


