//
//  ActivityParm.swift
//  MySport
//
//  Created by lanou3g on 16/3/1.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

class ActivityParm: NSObject,ParamProtocol{
    @objc var URLString:String {
        return "http://runmate.runtogether.cn/api/v2/onlineactivitys?page=0&pageSize=20"
    }
    var paramDic:[String:String]? {
        return nil
    }
    
    //    同过数据请求返回json（大字典或数组）后，将其转换为包含（可转化为模型的）字典的数组
    func dictionaryArray(json:AnyObject) ->[[String:AnyObject]]?{
        if let obj = json as? [String:AnyObject]{
            let arr = json["content"]
            return (arr as? [[String:AnyObject]])
        }
        return nil
    }
}
