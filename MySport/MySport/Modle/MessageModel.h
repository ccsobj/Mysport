//
//  MessageModel.h
//  发现板块  资讯类
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 lanou3g. All rights reserved.
//

#import <Foundation/Foundation.h>

//#define kURL @"http://api.fit-time.cn/ftinfo/getInfoByIds?stat=1&id=440&id=439&id=438&id=436&id=435&token=hedpk78qtzL9mtY9NMdpr8jRAIKKzn9eUW71BT7ugcdsrFTIr%2BerBw%3D%3D&device_id=F9AACD9C-F5FF-4320-B162-2ED01B544B2C&client=ios&idfa=C035B1B9-CECD-48CE-BCCF-41E2877D853A&lat=40.03049915124289&lon=116.3434795770147&adcode=110108&os=iPhone%20OS%209.2.1&model=iPhone8%2C1&ver=2.5.1"

@interface MessageModel : NSObject
@property (nonatomic ,strong)NSString *title;//标题名
@property (nonatomic ,strong)NSNumber *praiseCount;//点赞人数
@property (nonatomic ,strong)NSString *photo;//图片地址
@property (nonatomic ,strong)NSString *url;//详情页面地址
@property (nonatomic ,strong)NSString *startTime;//开始时间




@end
