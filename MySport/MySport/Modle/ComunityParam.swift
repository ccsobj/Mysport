//
//  ComunityParam.swift
//  MySport
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

class ComunityParam:NSObject, ParamProtocol {
    
    @objc var URLString:String {
        return "http://api.fit-time.cn/ftsns/refreshUserFeed?page_size=20&elite=1&photo_only=1&token=1&client=ios&idfa=C035B1B9-CECD-48CE-BCCF-41E2877D853A&lat=40.03052845360504&lon=116.3434497016181&adcode=110108&os=iPhone%20OS%209.2.1&model=iPhone8%2C1&ver=2.5.1"
    }
    
    var paramDic:[String:String]? {
        return nil
    }
    
    func dictionaryArray(json: AnyObject) -> [[String : AnyObject]]? {
        if let obj = json as? [String:AnyObject]{
            let arr = json["feeds"]
            return (arr as? [[String:AnyObject]])
        }
        return nil
    }
}
