//
//  MainCollectionView.swift
//  WangYiNews
//
//  Created by lanou3g on 16/2/28.
//  Copyright © 2016年 lanou3g. All rights reserved.
//

import UIKit 

import Async

protocol TableConposedCollectionViewDelegate:class {
    var navigationController: UINavigationController? { get }
    func tableConposedCollectionView(collectionView:TableConposedCollectionView , didChangeWithIndex index:Int)
}

@objc class TableConposedCollectionView: UICollectionView,UICollectionViewDelegate,UICollectionViewDataSource{
//  当collectionView的代理，用于控制切换cell时的动作
    weak var changeDelegate : TableConposedCollectionViewDelegate?
//    判断itemsize是否已经被成功设置
    var itemSizeHaveSet : Bool = false
//    cell上tableview的控制器
    let tableVCs : [PushableProtocol] = [CommunityTableViewController(),MessageTableViewController(),ActivityTableViewController()]
    
    var numberOfItems : Int{
        return tableVCs.count
    }
    
    init(delegate:TableConposedCollectionViewDelegate){
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .Horizontal
        self.changeDelegate = delegate
        super.init(frame:CGRect.zero, collectionViewLayout:layout)
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        self.pagingEnabled = true
        self.bounces = false
        self.delegate = self
        self.dataSource = self
        self.registerClass(TableConposedCollectionViewCell.self, forCellWithReuseIdentifier:"cell")
        self.contentOffset = CGPoint.zero
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func getIndex(){}
    
    func updateWithIndex(index:Int){
        let x = CGFloat(index) * self.bounds.size.width
        self.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
   //MARK  collectinView的代理方法
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath:indexPath) as! TableConposedCollectionViewCell
//        self.tableVCs.map(){   }
        let vc = self.tableVCs[indexPath.row]
        vc.navVC = self.changeDelegate?.navigationController
        cell.setupTableView(vc.tableView)
        return cell
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let index = scrollView.contentOffset.x / self.bounds.size.width
        self.changeDelegate?.tableConposedCollectionView(self, didChangeWithIndex:Int(index))
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.numberOfItems
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if itemSizeHaveSet { return }
        if self.bounds.size.width < UIScreen.mainScreen().bounds.size.width { return }
        if let layout = self.collectionViewLayout as? UICollectionViewFlowLayout{
            var once : dispatch_once_t = 0
            dispatch_once(&once){
                layout.itemSize = self.bounds.size
                self.itemSizeHaveSet = true
           dispatch_async(dispatch_get_main_queue()){
                    self.reloadItemsAtIndexPaths([NSIndexPath(index:0)])
                }
            }
        }
    }
}
