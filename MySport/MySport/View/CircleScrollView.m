
//
//  CircleScrollViewController.m
//  CircleScrollVIew
//
// 轮播图视图控件

#import "CircleScrollView.h"
#import "ImageBrowsController.h"
#import "UIImageView+WebCache.h"
#import <MySport-swift.h>


#define CIRCLESCROLLVIEW_PLACEHOLDER  @"placehoder"
#define SCREEN_WITH    _scollView.bounds.size.width
#define SCREEN_HEIGHT  _scollView.bounds.size.height

#define SCROLL_DURATION  4.5

@interface CircleScrollView () <UIScrollViewDelegate,UIScrollViewAccessibilityDelegate>

@property (nonatomic,strong)NSMutableArray *images;

@property (nonatomic,strong)UIScrollView *scollView;

@property (nonatomic,strong)NSTimer *time;
@property (nonatomic,assign)NSTimeInterval duration;
@property (nonatomic,assign,getter=isplaying)BOOL playing;

@property (nonatomic,strong)UIImageView *imgView0;
@property (nonatomic,strong)UIImageView *imgView;
@property (nonatomic,strong)UIImageView *imgView1;
@property (nonatomic,strong)UIPageControl *pageControl;
@property (nonatomic,strong)NSArray *urls;

@property (strong,nonatomic)UIImageView *palceHolderView;



@property (nonatomic,copy)void(^redrectBlock)(NSInteger);


@property (nonatomic,strong)ImageBrowsController *vc;


@end

@implementation CircleScrollView


- (NSMutableArray *)images
{
    if (_images == nil) {
        _images = [NSMutableArray array];
    }
    return  _images;
}

- (NSInteger)currentIdex
{
    return [self safeIndex:_currentIdex];
}

- (NSInteger)safeIndex:(NSInteger)index{
    NSInteger count = 0 ;
    if (self.urls.count > 0) {
        count = self.urls.count;
    }else if(self.images.count > 0){
        count = self.images.count;
    }
    if (index < 0) {
        index = count + index;
    }else if (index >= count ) {
        index = index - count;
    }
    return index;
}

#pragma mark 重写初始化方法
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        UIImage *palceImage = [UIImage imageNamed:CIRCLESCROLLVIEW_PLACEHOLDER];
        [self setScollViewWithFrame:self.bounds];
        [self setupImageViewWithImages:@[palceImage,palceImage,palceImage]];
        [_scollView addSubview:_imgView0];
        [_scollView addSubview:_imgView];
        [_scollView addSubview:_imgView1];
        [self addSubview:_scollView];
    }
    return  self;
}

//  scrollerView的初始化
- (void)setScollViewWithFrame:(CGRect)frame
{
    //    初始化并设置滚动视图
    _scollView = [[UIScrollView alloc]initWithFrame:frame];
    _scollView.contentSize = CGSizeMake(SCREEN_WITH, SCREEN_HEIGHT);
    _scollView.contentOffset = CGPointMake(SCREEN_WITH, 0);
    _scollView.pagingEnabled = YES;
    _scollView.scrollEnabled = NO;
    _scollView.showsHorizontalScrollIndicator = NO;
    _scollView.showsVerticalScrollIndicator = NO;
    _scollView.delegate = self;
}

//   设置三张imageView的尺寸
- (void)setupImageViewWithImages:(NSArray<UIImage *> *)images
{
    _imgView0 = [[UIImageView alloc]initWithImage:images[0]];
    _imgView = [[UIImageView alloc]initWithImage:images[1]];
    _imgView1 = [[UIImageView alloc]initWithImage:images[2]];
    
    _imgView0.frame = CGRectMake(0, 0, SCREEN_WITH, SCREEN_HEIGHT);
    _imgView.frame = CGRectMake(SCREEN_WITH, 0, self.bounds.size.width, self.bounds.size.height);
    _imgView1.frame = CGRectMake(SCREEN_WITH * 2, 0, self.bounds.size.width, self.bounds.size.height);
    //    为中部图片添加手势，以便点击后可以进入图片浏览器
    _imgView.userInteractionEnabled = true;
    UITapGestureRecognizer *tap =  [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(junpToPhotoBrowns:)];
    [_imgView addGestureRecognizer:tap];
}
// 跳转到图片浏览器
- (void)junpToPhotoBrowns:(UITapGestureRecognizer *)tap
{
    [self.delegate circleScrollView:self didSelectedPage:self.currentIdex sourceView:self.imgView img:self.imgView.image];
}

#pragma mark 根据图片url更新视图

- (void)updateWithImageUrls:(NSArray<NSString *> *)urls
{
    [self updateWithUrls:urls Douration:SCROLL_DURATION completeHandle:^(NSInteger index){}];
}

- (void)updateWithUrls:(NSArray *)urls Douration:(NSTimeInterval)douration completeHandle:(void(^)(NSInteger index))block
{
    self.urls = urls;
    self.currentIdex = 0;
    self.playing = NO;
    self.duration = douration;
    [self updateImage];
    
    if (urls.count <= 1) {
        self.scollView.scrollEnabled = NO;
    }else{
        _scollView.contentSize = CGSizeMake(SCREEN_WITH * 3, SCREEN_HEIGHT);
        self.scollView.scrollEnabled = YES;
        if (douration == 0) {
            return ;
        }
        [self playTimeWithTimInterval:self.duration repeats:YES];
    }
}


#pragma mark 便利初始化方法
- (instancetype)initWithFrame:(CGRect)frame URLs:(NSArray *)urls Douration:(NSTimeInterval)douration
{
    if (self = [super initWithFrame:frame]) {
        self.urls = urls;
        self.currentIdex = 0;
        self.playing = NO;
        self.duration = douration;
        [self setScollViewWithFrame:self.frame];
        if (urls.count <= 1) {
            self.scollView.scrollEnabled = NO;
        }else{
            [self playTimeWithTimInterval:self.duration repeats:YES];
        }
    }
    return self;
}

#pragma mark 更新srollerView
- (void)refreshScrollView
{
    CGFloat x = _scollView.bounds.size.width;
    self.scollView.contentOffset = CGPointMake(x, 0);
    self.pageControl.currentPage = self.currentIdex;
    [self updateImage];
}

- (void)updateImage
{
    if (self.urls.count > 0) {
        [self.imgView0 sd_setImageWithURL:_urls[[self safeIndex:self.currentIdex -1]]];
        [self.imgView sd_setImageWithURL:_urls[[self safeIndex:self.currentIdex]]];
        [self.imgView1 sd_setImageWithURL:_urls[[self safeIndex:self.currentIdex + 1]]];
    }else if(self.images.count > 0){
        self
        .imgView0.image = _images[[self safeIndex:self.currentIdex - 1]];
        self.imgView.image = _images[[self safeIndex:self.currentIdex]];
        self.imgView1.image = _images[[self safeIndex:self.currentIdex + 1]];
    }
}

#pragma mark 设置播放时间，是否重复；默认不开启播放
- (void)playTimeWithTimInterval:(NSTimeInterval)timeInterval repeats:(BOOL)isRepeats
{
    if (self.urls.count > 1) {
        self.duration = timeInterval;
        self.playing = YES;
        [self.time invalidate];
        self.time = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(timeUpdateToScrollerView) userInfo:nil repeats:isRepeats];
        [[NSRunLoop mainRunLoop] addTimer:self.time forMode:NSRunLoopCommonModes];
    }
}
- (void)timeUpdateToScrollerView
{
    [UIView animateWithDuration:0.5 animations:^{
        [self.scollView setContentOffset:CGPointMake(SCREEN_WITH * 2, 0.0) animated:NO];
    } completion:^(BOOL finished) {
        self.currentIdex++;
        [self refreshScrollView];
    }];
}

#pragma mark scrollerView的代理方法

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    if (self.images.count <= 1 && self.urls.count <= 1) {
        return;
    }
    CGFloat page = scrollView.contentOffset.x / self.bounds.size.width;
    if (page == 2) {
        self.currentIdex++;
        [self refreshScrollView];
    }
    if (page == 0) {
        self.currentIdex--;
        [self refreshScrollView];
    }
}

//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    if (self.isplaying == YES) {
//        self.time = [NSTimer scheduledTimerWithTimeInterval:_duration target:self selector:@selector(timeUpdateToScrollerView) userInfo:nil repeats:YES];
//        [[NSRunLoop mainRunLoop] addTimer:self.time forMode:NSRunLoopCommonModes];
//    }
//}
//
//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//    [self.time invalidate];
//}








@end
