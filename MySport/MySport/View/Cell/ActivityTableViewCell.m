//
//  ActivityTableViewCell.m
//  MySport
//
//  Created by lanou3g on 16/3/1.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "ActivityTableViewCell.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#define kheight self.contentView.frame.size.height
#define kwidth self.contentView.frame.size.width
@implementation ActivityTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addviews];
    }return self;
}



-(void)addviews{

    //底视图
    //图片
    self.image = [[UIImageView alloc] init];
    self.image.backgroundColor = [UIColor orangeColor];
    [self.contentView addSubview:self.image];
    [self.image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).width.insets(UIEdgeInsetsMake(0,0,0,0));
    }];
    //标题
    self.headName = [[UILabel alloc] init];
    self.headName.numberOfLines = 0;
    self.headName.textColor = [UIColor whiteColor];
    [self.contentView addSubview:self.headName];
    self.headName.font = [UIFont fontWithName:@"Superclarendon" size:20];
    self.headName.font = [UIFont boldSystemFontOfSize:20];
    [self.headName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(20);
        make.bottom.equalTo(self.contentView).with.offset(-50);

    }];
    
    //开始时间
    self.time = [[UILabel alloc] init];
    self.time.text = @"活动开始时间:";
    self.time.textColor = [UIColor grayColor];
    [self.contentView addSubview:self.time];
    [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(0);
        make.bottom.equalTo(self.contentView).with.offset(-15);
        

 }];
    //固定参加人数
    self.participation = [[UILabel alloc] init];
    self.participation.text = @"参与人数:";
        self.participation.numberOfLines = YES;
    self.participation.font = [UIFont systemFontOfSize:15.0];
    [self.contentView addSubview:self.participation];
    [self.participation mas_makeConstraints:^(MASConstraintMaker *make) {

        make.right.equalTo(self.contentView).with.offset(-45);
        make.bottom.equalTo(self.contentView).with.offset(-15);

        
    }];
        
    //活动
    self.activity = [[UILabel alloc] init];
    self.activity.text = @"活动";
    [self.contentView addSubview:self.activity];
    [self.activity mas_makeConstraints:^(MASConstraintMaker *make) {

          make.right.equalTo(self.contentView).with.offset(-20);
          make.top.equalTo(self.contentView).with.offset(20);
        
    }];
     
    //参加人数
    self.number = [[UILabel alloc] init];
    self.number.backgroundColor = [UIColor brownColor];
    [self.contentView addSubview:self.number];
    [self.number mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).with.offset(-10);
        make.bottom.equalTo(self.contentView).with.offset(-15);

    }];
    
    //开始时间
    self.startTime = [[UILabel alloc] init];
    [self.contentView addSubview:self.startTime];
    [self.startTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.time).with.offset(105);
        make.bottom.equalTo(self.contentView).with.offset(-15);
        
    }];
}


- (void)updteWithActivity:(ActivityModel *)Activity{
    
    NSString *strurl = [NSString stringWithFormat:@"%@",Activity.headImage];
    [self.image sd_setImageWithURL:[NSURL URLWithString:strurl] placeholderImage:[UIImage imageNamed:@"1"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    self.headName.text = Activity.headName;
    self.number.text = [NSString stringWithFormat:@"%@",Activity.joinNum];
    NSDateFormatter *fotmatter = [[NSDateFormatter alloc] init];
    [fotmatter setDateFormat:@"yyyy-MM-dd"];
    NSString *string = [NSString stringWithFormat:@"%@",Activity.startTime];
    NSTimeInterval time=[string doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time/1000];
    NSString *timestr = [fotmatter stringFromDate:detaildate];
    self.startTime.text =timestr;

    
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
