//
//  MainCollectionViewCell.swift
//  WangYiNews
//
//  Created by lanou3g on 16/2/28.
//  Copyright © 2016年 lanou3g. All rights reserved.
//

import UIKit

class TableConposedCollectionViewCell: UICollectionViewCell{
//    weak var tableView:HomeTableView?
//    var tableViewController : UITableViewController?
    
    func setupTableView(tableView:UITableView){
        var once : dispatch_once_t = 0
        dispatch_once(&once){
//            self.tableViewController = self.tableViewControllerWithIndex(index)
            tableView.frame = self.bounds
            self.addSubview(tableView)
        }
    }
    
    private  func tableViewControllerWithIndex(index:Int)->UITableViewController{
        switch index{
        case 0 : return CommunityTableViewController()
        case 1 : return MessageTableViewController()
        case 2 : return ActivityTableViewController()
        default: break
        }
        return UITableViewController()
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        if let view = self.tableViewController?.view{
//            view.frame = self.bounds
//        }
//    }
}
