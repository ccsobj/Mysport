//
//  CommunityTableViewCell.swift
//  MySport
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

class CommunityTableViewCell: UITableViewCell {

    private lazy var iconView : UIImageView = {
        let iconview = UIImageView()
        return iconview
    }()
    
    private lazy var nameLable : UILabel = {
        return UILabel()
    }()
    
    private lazy var timeLable : UILabel = {
        return UILabel()
    }()
    
    private lazy var cententLable : UILabel = {
        return UILabel()
    }()
    
    private lazy var focuslable : UILabel = {
        return UILabel()
    }()
    
    private var toolBar : CommunityToolBar = {
        return CommunityToolBar()
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier:reuseIdentifier)
        self.addSubview(iconView)
        self.addSubview(timeLable)
        self.addSubview(nameLable)
        self.addSubview(toolBar)
        self.addSubview(contentView)
        self.addSubview(focuslable)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
}
