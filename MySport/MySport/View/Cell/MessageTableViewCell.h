//
//  MessageTableViewCell.h
//  发现板块  资讯类
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageModel.h"

@interface MessageTableViewCell : UITableViewCell

@property (nonatomic ,strong)UIImageView *image;//资讯背景图
@property (nonatomic ,strong)UILabel *titlelabel;//标题
@property (nonatomic ,strong)UILabel *timelabel;//时间
@property (nonatomic ,strong)UIButton *praisebut;//点赞按钮
@property (nonatomic ,strong)UILabel *praise;//点赞人数

@property (nonatomic ,assign)BOOL *bl;//判断属性

- (void)updteWithMessage:(MessageModel *)message;

@end
