//
//  ActivityTableViewCell.h
//  MySport
//
//  Created by lanou3g on 16/3/1.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityModel.h"
@interface ActivityTableViewCell : UITableViewCell

@property (nonatomic ,strong)UILabel *headName;//标题
@property (nonatomic ,strong)UIImageView *image;//底视图
@property (nonatomic ,strong)UILabel *time;//距离活动结束和开始时间
@property (nonatomic ,strong)UILabel *participation;//参加
@property (nonatomic ,strong)UILabel *number;//参加人数
@property (nonatomic ,strong)UILabel *activity;//活动
@property (nonatomic ,strong)UILabel *startTime;//开始时间


- (void)updteWithActivity:(ActivityModel *)Activity;



@end
