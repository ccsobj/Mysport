//
//  MessageTableViewCell.m
//  发现板块  资讯类
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 lanou3g. All rights reserved.
//

#import "MessageTableViewCell.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import <MySport-swift.h>
#define kwidth self.contentView.bounds.size.width
#define kheight self.contentView.bounds.size.height
@implementation MessageTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addviews];
    }return self;
}

-(void)addviews{
    //图片
    self.image = [[UIImageView alloc] init];
    [self.contentView addSubview:self.image];
    [self.image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).width.insets(UIEdgeInsetsMake(20,0,0,0));
    }];
    //标题
    self.titlelabel = [[UILabel alloc] init];
    [self.contentView addSubview:self.titlelabel];
    [self.titlelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).width.insets(UIEdgeInsetsMake(150,10,20,10));
    }];
    //时间
    self.timelabel = [[UILabel alloc] init];
    [self.contentView addSubview:self.timelabel];
    [self.timelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).width.insets(UIEdgeInsetsMake(0,20,180,200));
    }];
    //点赞按钮
    self.praisebut = [[UIButton alloc] init];
    [self.praisebut setTitle:@"赞" forState:UIControlStateNormal];
//    [self.praisebut setImage:[UIImage imageNamed:@"praise"] forState:UIControlStateNormal];
    [self.contentView addSubview:self.praisebut];
  [self.praisebut addTarget:self action:@selector(praiseAction:) forControlEvents:(UIControlEventTouchUpInside)];

    
    //点赞人数
    self.praise = [[UILabel alloc] init];
    self.praise.backgroundColor = [UIColor grayColor];
    self.praise.alpha = 0.7;
    
    [self.contentView addSubview:self.praise];

    self.praise.frame = CGRectMake(self.contentView.bounds.size.width * 1.2, 35, 40, 30);
}
- (void)updteWithMessage:(MessageModel *)message{
    self.titlelabel.text = message.title;
    self.praise.text = [NSString stringWithFormat:@"%@",message.praiseCount];
    
    NSString *strurl = [NSString stringWithFormat:@"http://%@",message.photo];
    [self.image sd_setImageWithURL:[NSURL URLWithString:strurl] placeholderImage:[UIImage imageNamed:@"1"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
    NSDateFormatter *fotmatter = [[NSDateFormatter alloc] init];
    [fotmatter setDateFormat:@"yyyy-MM-dd"];
    NSString *string = [NSString stringWithFormat:@"%@",message.startTime];
    NSTimeInterval time=[string doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time/1000];
    NSString *timestr = [fotmatter stringFromDate:detaildate];
    self.timelabel.text =timestr;
    
}
-(void)layoutSubviews{
    [super layoutSubviews];
    self.praisebut.frame = CGRectMake(kwidth * 0.75, kheight * 0.2, kwidth * 0.1, kheight * 0.1);
    self.praise.frame = CGRectMake(kwidth * 0.9,  kheight * 0.2, kwidth * 0.2, kheight * 0.1);

}

-(void)praiseAction:(UIButton *)sender{
    [self.praisebut setTitle:@"赞" forState:UIControlStateHighlighted];
//    [sender setImage:[UIImage imageNamed:@"praise1"] forState:UIControlStateNormal];

}

@end
