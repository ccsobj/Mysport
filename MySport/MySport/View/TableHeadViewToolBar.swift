//
//  AdsButtomView.swift
//  WangYiNews
//
//  Created by lanou3g on 16/1/19.
//  Copyright © 2016年 lanou3g. All rights reserved.
//

import UIKit

// 轮播图底部（之上）pagecontrol ＋ 标题 复合视图
class TableHeadViewToolBar: UIView {
    
    let pageControl = UIPageControl()
    let imgViwe = UIImageView()
    let titleLable = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        self.addSubview(self.pageControl)
        self.addSubview(self.imgViwe)
        self.addSubview(self.titleLable)
        self.setNeedsLayout()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imgViwe.frame = CGRect(origin:CGPoint(x:0, y: 0), size:CGSize(width:50, height:self.bounds.width))
        self.titleLable.frame = CGRect(x:self.imgViwe.frame.maxX, y:0, width:80, height:self.frame.size.width)
    }
}
