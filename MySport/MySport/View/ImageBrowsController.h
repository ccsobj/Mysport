//
//  ImageBrowsController.h
//  微博
//
//  Created by lanou3g on 15/12/17.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageBrowsController : UIViewController

@property (nonatomic,strong)NSArray *photo_urls;
@property (nonatomic,assign)NSInteger currrentIndex;
//@property (nonatomic,strong)UIImageView *sourceView;

- (void)showWithSouceView:(UIImageView *)souceView PhotoUrls:(NSArray *)pthto_urls;


- (void)showWithImage:(UIImage *)image PhotoUrls:(NSArray *)urls souceView:(UIView *)souceView;

@end
