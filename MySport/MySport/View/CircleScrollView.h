//
//  CircleScrollViewController.h
//  CircleScrollVIew
//
//  Created by lanou3g on 15/12/8.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>



@class CircleScrollView;
@protocol CircleScrollViewDelegate <NSObject>

- (void)circleScrollView:(CircleScrollView *)circleView didSelectedPage:(NSInteger)page sourceView:(UIImageView *)souceView img:(UIImage *)img;

@end

@interface CircleScrollView : UIView

@property (nonatomic,weak)id<CircleScrollViewDelegate> delegate;

@property (nonatomic,assign)NSInteger currentIdex;


- (void)updateWithImageUrls:(NSArray<NSString *> *)urls;

- (void)updateWithUrls:(NSArray *)urls Douration:(NSTimeInterval)douration completeHandle:(void(^)(NSInteger index))block;


@end
