//
//  TopAdsView.swift
//  WangYiNews
//
//  Created by lanou3g on 16/1/19.
//  Copyright © 2016年 lanou3g. All rights reserved.
//   tableView 的头视图 轮播图

import UIKit

let kButtomHeight:CGFloat = 30.0

let TABLEHEADVIEW_HEIGHT:CGFloat = 200.0

class TableHeadView: UIView,UIScrollViewDelegate,CircleScrollViewDelegate{
    
//    轮播图下方的pagecontrol ＋ 标题标签
    let buttomView = TableHeadViewToolBar()

//  轮播图主体控件
    private  var circleScrollView : CircleScrollView!
//    图片浏览器，点击图片后进入
   private  var imageBrowsVC : ImageBrowsController?
    
//    轮播的间隔时间
    let repeatDuration:Double = 8.0
    
//    当前的图片下标
    var currentPage:Int = 0
    
    
    init(){
        super.init(frame:CGRect(x: 0, y: 0, width:UIScreen.mainScreen().bounds.size.width, height:TABLEHEADVIEW_HEIGHT))
        self.circleScrollView = CircleScrollView(frame:self.bounds)
        self.addSubview(self.circleScrollView)
        self.circleScrollView.delegate = self;
        self.setupButtomView()
    }
    
//    轮播图数据模型
    override init(frame: CGRect) {
        super.init(frame:frame)
        self.circleScrollView = CircleScrollView(frame:self.bounds)
        self.addSubview(self.circleScrollView)
        self.circleScrollView.delegate = self;
        self.setupButtomView()
    }
    
    
    func updateWithImageUrls(urls:[String]){
        self.circleScrollView.updateWithImageUrls(urls)
    }
    
    private func setupButtomView(){
        let y = self.bounds.size.height - kButtomHeight
        self.buttomView.frame = CGRect(x:0, y:y, width:self.bounds.size.width, height:kButtomHeight)
        self.addSubview(self.buttomView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
//    circleScrollView代理方法
    func circleScrollView(circleView: CircleScrollView!, didSelectedPage page: Int, sourceView souceView: UIImageView!,img:UIImage) {
        if let urls = urlsWithIndex(page){
            self.imageBrowsVC = ImageBrowsController()
            self.imageBrowsVC!.showWithImage(img, photoUrls:urls, souceView:self)
        }
    }
    
//    通过下标获取（清晰）图片的url数组
    private func urlsWithIndex(index:Int)->[String]?{
        return [""]
    }
    
}

extension UIImageView{
    func updateWithUrl(urlString:String){
        let url = NSURL(string:urlString)
        let image = UIImage(named:"navbar_netease")
        self.sd_setImageWithURL(url, placeholderImage:image)
    }
}

