//
//  ImageBrowsController.m
//  微博
//
//  Created by lanou3g on 15/12/17.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "ImageBrowsController.h"

#import "UIImageView+WebCache.h"


@interface ImageBrowsController ()<UIScrollViewDelegate>

@property (nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,assign)CGRect lastFrame;
@property (nonatomic,strong)UILabel *pageLable;

@property (nonatomic,assign)CGSize imageSize;


@end

@implementation ImageBrowsController

- (instancetype)init
{
    if (self = [super init]) {
        self.view.backgroundColor = [UIColor blackColor];
        _scrollView = [[UIScrollView alloc]initWithFrame:self.view.frame];
        _scrollView.pagingEnabled = YES;
        _scrollView.userInteractionEnabled = YES;
        _scrollView.delegate = self;
        [self.view addSubview:_scrollView];
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat heiht = 50;
        CGFloat x = width / 2;
        CGFloat y = 80;
        self.pageLable = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, heiht)];
        self.pageLable.textColor = [UIColor purpleColor];
        self.pageLable.font = [UIFont systemFontOfSize:22];
//        self.navigationController
        [self.view addSubview:self.pageLable];
    }
    return self;
}

- (void)showWithImage:(UIImage *)image PhotoUrls:(NSArray *)urls souceView:(UIView *)souceView
{

    _lastFrame = souceView.frame;
    _photo_urls = urls;
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = width * image.size.width / image.size.height;
    self.imageSize = CGSizeMake(width,height);
    
    _pageLable.text = [NSString stringWithFormat:@"%ld/%ld页",_currrentIndex + 1,_photo_urls.count];
    [_pageLable sizeToFit];
    [self setupScrollView];
    __block UIImageView *newView = [[UIImageView alloc]init];
    newView.frame = [self.view convertRect:souceView.frame fromView:souceView.superview];
    _lastFrame = newView.frame;
    [UIView animateWithDuration:0.25 animations:^{
        CGFloat newView_width = [UIScreen mainScreen].bounds.size.width;
        CGFloat newView_height = newView_width * image.size.width / image.size.height;
        CGFloat newView_y = ([UIScreen mainScreen].bounds.size.height - newView_height) / 2;
        newView.frame = CGRectMake(0,newView_y, newView_width, newView_height);
        _scrollView.contentOffset = CGPointMake(newView_width * _currrentIndex, 0);
    } completion:^(BOOL finished) {
        [newView removeFromSuperview];
        newView = nil;
        [self.view addSubview:newView];
        UIWindow *window = [UIApplication sharedApplication].windows.lastObject;
        [window addSubview:self.view];
        self.view.hidden = NO;
    }];
}


- (void)setupScrollView
{
    CGFloat contentW = self.view.frame.size.width * _photo_urls.count;
    CGFloat contentH = self.view.frame.size.height;
    _scrollView.contentSize = CGSizeMake(contentW, contentH);
    for (int i = 0; i < _photo_urls.count; i++) {
       __block CGFloat x = self.view.frame.size.width  * i;
        CGFloat width = self.view.frame.size.width;
        CGFloat height = self.view.frame.size.height;
      __block  UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(x, 0, width, height)];
        imageView.userInteractionEnabled = YES;
        
        UIImage * image = [UIImage imageNamed:@"ad"];
        
        [imageView sd_setImageWithURL:self.photo_urls[i] placeholderImage:image completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            CGFloat newView_width = [UIScreen mainScreen].bounds.size.width;
            CGFloat newView_height = newView_width * image.size.width / image.size.height;
            CGFloat newView_y = ([UIScreen mainScreen].bounds.size.height - newView_height) / 2;
            imageView.frame = CGRectMake(x,newView_y, newView_width, newView_height);
        }];
        [_scrollView addSubview:imageView];
        UITapGestureRecognizer *dackViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(back:)];
        [imageView addGestureRecognizer:dackViewTap];
    }
    CGFloat x = self.view.frame.size.width * _currrentIndex;
    _scrollView.contentOffset = CGPointMake(x, 0);
}

- (void)back:(UITapGestureRecognizer *)tap
{
    __block UIImageView *newView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width)];
    newView.image = [(UIImageView *)tap.view image];
    [self.view addSubview:newView];
    self.scrollView = nil;
    [UIView animateWithDuration:0.1 animations:^{
        newView.frame = _lastFrame;
    } completion:^(BOOL finished) {
        [newView removeFromSuperview];
        newView = nil;
        [self.view removeFromSuperview];
        self.photo_urls = nil;
        [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    _currrentIndex = (NSInteger)scrollView.contentOffset.x / self.view.frame.size.width;
    _pageLable.text = [NSString stringWithFormat:@"%ld/%ld页",_currrentIndex + 1,_photo_urls.count];
    [_pageLable sizeToFit];
}

@end
