//
//  LoginView.h
//  Log
//
//  Created by lanou3g on 16/2/27.
//  Copyright © 2016年 王正权. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginView : UIView

@property(nonatomic,strong)UITextField *IDField;

@property(nonatomic,strong)UIImageView *imagv;

@property(nonatomic,strong)UITextField *passwordField;

@property(nonatomic,strong)UIImageView *imagv1;

@property(nonatomic,strong)UILabel *otherLabel;

@property(nonatomic,strong)UIButton *weixinBtn;

@property(nonatomic,strong)UIButton *qqBtn;

@property(nonatomic,strong)UIButton *weiboBtn;

@property(nonatomic,strong)UIButton *dengluBtn;

@property(nonatomic,strong)UIButton *findBtn;

@property(nonatomic,strong)UIButton *registerBtn;

@property (nonatomic,strong)UIButton *travelBtn;

@end
