//
//  LoginView.m
//  Log
//
//  Created by lanou3g on 16/2/27.
//  Copyright © 2016年 王正权. All rights reserved.
//

#import "LoginView.h"
#import "Masonry.h"

@implementation LoginView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        [self addView];
    }
    return self;
}



-(void)addView{
    
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg4"]];
    
   // *账号名
    self.IDField = [[UITextField alloc]init];
    self.IDField.backgroundColor = [UIColor whiteColor];
   self.IDField.borderStyle = UITextBorderStyleRoundedRect;//外框类型
    self.IDField.clearButtonMode = UITextFieldViewModeAlways; //编辑时会出现个修改X
    self.IDField.placeholder = @"约跑账号/QQ邮箱/手机号";
    // *账户名小头像
    self.imagv = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_profile_normal"]];
//    self.imagv.frame = CGRectMake(0, 0, 0, 20);
    self.IDField.leftView = self.imagv;
    self.IDField.leftViewMode = UITextFieldViewModeAlways;
    [self addSubview:self.imagv];
    [self addSubview:self.IDField];
    [self.IDField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(35);
        make.left.mas_equalTo(40);
        make.top.mas_equalTo(100);
        make.right.mas_equalTo(-30);
    }];
    [self.imagv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.IDField);
        
    }];
    
    // *密码
    self.passwordField = [[UITextField alloc]init];
    self.passwordField.backgroundColor = [UIColor whiteColor];
    self.passwordField.borderStyle = UITextBorderStyleRoundedRect;
    self.passwordField.clearButtonMode = UITextFieldViewModeAlways;
    self.passwordField.placeholder = @"请输入密码";
    self.imagv1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_profile_normal"]];
    self.passwordField.leftView = self.imagv1;
    self.passwordField.leftViewMode = UITextFieldViewModeAlways;
    self.passwordField.secureTextEntry = YES;// 密文输入
    [self addSubview:self.imagv1];
    [self addSubview:self.passwordField];
    
    [self.passwordField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.IDField);
        make.left.mas_equalTo(self.IDField);
        make.top.mas_equalTo(self.IDField.mas_bottom).offset(10);
    }];
    [self.imagv1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.passwordField);
        
    }];
    
    self.otherLabel = [[UILabel alloc]init];
    self.otherLabel.text = @"第三方账号快速登陆";
    self.otherLabel.textColor = [UIColor grayColor];
    self.otherLabel.textAlignment = NSTextAlignmentCenter;
    self.otherLabel.font = [UIFont systemFontOfSize:18];
    [self addSubview:self.otherLabel];
    [self.otherLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(180, 30));
        make.left.mas_equalTo(100);
        make.top.mas_equalTo(self.passwordField.mas_topMargin).offset(250);
    }];
    
    
    self.weixinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.weixinBtn.layer setMasksToBounds:YES];
    [self.weixinBtn setImage:[UIImage imageNamed:@"weixin"] forState:UIControlStateNormal];
    [self.weixinBtn.layer setCornerRadius:25.0];
    [self addSubview:self.weixinBtn];
    [self.weixinBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(50, 50));
      make.top.mas_equalTo(self.passwordField.mas_topMargin).offset(300);
        make.left.mas_equalTo(100);
    }];
    
    self.qqBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.qqBtn.layer setMasksToBounds:YES];
    [self.qqBtn setImage:[UIImage imageNamed:@"QQ"] forState:UIControlStateNormal];
 
    [self.qqBtn.layer setCornerRadius:25.0];
    [self addSubview:self.qqBtn];
    [self.qqBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.weixinBtn);
        make.left.mas_equalTo(self.weixinBtn.mas_leftMargin).offset(65);
        make.top.mas_equalTo(self.passwordField.mas_topMargin).offset(300);
        
    }];
    
    self.weiboBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.weiboBtn.layer setMasksToBounds:YES];
    [self.weiboBtn setImage:[UIImage imageNamed:@"weibo"] forState:UIControlStateNormal];
    [self.weiboBtn.layer setCornerRadius:25.0];
    [self addSubview:self.weiboBtn];
    [self.weiboBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.qqBtn);
        make.left.mas_equalTo(self.qqBtn.mas_leftMargin).offset(65);
        make.top.mas_equalTo(self.passwordField.mas_topMargin).offset(300);
    }];
    
    self.dengluBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.dengluBtn.backgroundColor = [UIColor colorWithRed:150/255.0f green:150/255.0f blue:150/255.0f alpha:1];
    [self.dengluBtn setTitle:@"登陆" forState:UIControlStateNormal];
    self.dengluBtn.layer.cornerRadius = 6.0f;
    [self addSubview:self.dengluBtn];
    [self.dengluBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.passwordField);
        make.left.mas_equalTo(self.passwordField);
        make.top.mas_equalTo(self.passwordField.mas_bottom).offset(10);
    }];
    
    self.findBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.findBtn.backgroundColor = [UIColor colorWithRed:150/255.0f green:150/255.0f blue:150/255.0f alpha:1];
    [self.findBtn setTitle:@"找回密码" forState:(UIControlStateNormal)];
    self.findBtn.layer.cornerRadius = 6.0f;
    self.findBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:self.findBtn];
    [self.findBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 20));
        make.left.mas_equalTo(self.dengluBtn);
        make.top.mas_equalTo(self.dengluBtn.mas_bottom).offset(10);
    }];
    
    
    self.registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.registerBtn.backgroundColor = [UIColor colorWithRed:150/255.0f green:150/255.0f blue:150/255.0f alpha:1];
    [self.registerBtn setTitle:@"快速注册" forState:(UIControlStateNormal)];
     self.registerBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    self.registerBtn.layer.cornerRadius = 6.0f;
    [self addSubview:self.registerBtn];
    [self.registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 20));
        make.right.mas_equalTo(self.dengluBtn);
        make.top.mas_equalTo(self.dengluBtn.mas_bottom).offset(10);
    }];
    
    
//    设置游客进入入口按钮
    self.travelBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.travelBtn setTitle:@"先试用，再注册" forState:UIControlStateNormal];
    
    [self addSubview:self.travelBtn];
    [self.travelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(120, 30));
        make.bottom.mas_equalTo(self.mas_bottom).offset(-50);
        make.centerX.mas_equalTo(self.mas_centerX);
        
    }];
}




@end
