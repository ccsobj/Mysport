//
//  AppDelegate.swift
//  MySport
//
//  Created by lanou3g on 16/2/25.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

let UM_APPKEY = "56d7e6d767e58e7948001825"
let QQ_APPID = "1104889639"
let QQ_APPKEY = "v8I6NLh2PKnG4M9k"
let UM_URL = "http://www.umeng.com/social"
let WEBCHAR_APPID = "wxd930ea5d5a258f4f"
let WEBCHAR_APPSECRECT = "db426a9829e4b49a0dcac7b4162da6b6"
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,CLLocationManagerDelegate{

    var window: UIWindow?

    let location:CLLocationManager = CLLocationManager()
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        self.window = UIWindow(frame:UIScreen.mainScreen().bounds)
        self.window?.rootViewController = LoginViewController()
        self.window?.makeKeyAndVisible()
        
//      申请healthkit访问权限，并开始同步数据
        HealthTool.authorizeHealthKit(){print("\($0)\($1)")}
//        与leacloude服务器建立连接
        LeancloudManage.initLeancloude()
//        初始化友盟SDK
        self.setupUMSDK()
        
        ClassMessage1(aclass:MessageModel.self)
        
        return true
    }
    
    private func setupUMSDK(){
        UMSocialData.setAppKey(UM_APPKEY)
        UMSocialQQHandler.setQQWithAppId(QQ_APPID, appKey:QQ_APPKEY, url:UM_URL)
        UMSocialWechatHandler.setWXAppId(WEBCHAR_APPID, appSecret:WEBCHAR_APPSECRECT, url:UM_URL)
    }
    
//    func application(application: UIApplication, handleOpenURL url: NSURL) -> Bool {
//        return UMSocialSnsService.handleOpenURL(url)
//    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.ccsobj.MySport" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("MySport", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}

