//
//  zhuceOneViewController.m
//  Log
//
//  Created by lanou3g on 16/2/27.
//  Copyright © 2016年 王正权. All rights reserved.
//

#import "zhuceOneViewController.h"
#import "zhuCeCompleteViewController.h"
#import <MySport-swift.h>
@interface zhuceOneViewController ()
{
    UIView *bgView;
    UITextField *phone;
    UITextField *code;
    UITextField *againFlied;
    UINavigationBar *customNavigationBar;
}

@end

@implementation zhuceOneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.title = @"注册3/1";
    self.navigationController.navigationBarHidden = NO;
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc]initWithTitle:@"返回" style:(UIBarButtonItemStyleDone) target:self action:@selector(dismiss)];
    self.view.backgroundColor=[UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:1];
   
    
    [self addview];
    
}

- (void)dismiss
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)addview{
    
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(30, 75, self.view.frame.size.width-90, 30)];
    label.text=@"请输入您的手机号码";
    label.textColor=[UIColor grayColor];
    label.textAlignment=NSTextAlignmentLeft;
    label.font=[UIFont systemFontOfSize:14];
    [self.view addSubview:label];
    
    CGRect frame = [UIScreen mainScreen].bounds;
    bgView = [[UIView alloc]initWithFrame:CGRectMake(10, 110, frame.size.width-20, 150)];
    bgView.layer.cornerRadius = 3.0;
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    

    phone = [ self createTextFielfFrame:CGRectMake(100, 10, 200, 30) font:[UIFont systemFontOfSize:14] placeholder:@"昵称"];
    phone.clearButtonMode = UITextFieldViewModeWhileEditing;
    phone.keyboardType=UIKeyboardTypeNumberPad;
    [bgView addSubview:phone];
    
    
    code =[self createTextFielfFrame:CGRectMake(100, 60, 90, 30) font:[UIFont systemFontOfSize:14]  placeholder:@"数字" ];
    code.clearButtonMode = UITextFieldViewModeWhileEditing;
    code.secureTextEntry=YES;//密文样式
    code.keyboardType=UIKeyboardTypeNumberPad;
    [bgView addSubview:code];
    
    againFlied =[self createTextFielfFrame:CGRectMake(100, 110, 200, 30) font:[UIFont systemFontOfSize:14]  placeholder:@"数字" ];
    againFlied.clearButtonMode = UITextFieldViewModeWhileEditing;
    againFlied.secureTextEntry=YES;//密文样式
    againFlied.keyboardType=UIKeyboardTypeNumberPad;
    [bgView addSubview:againFlied];
    
    UILabel *phonelabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 12, 100, 25)];
    phonelabel.text = @"账户昵称";
    phonelabel.textColor = [UIColor blackColor];
    phonelabel.textAlignment = NSTextAlignmentLeft;
    phonelabel.font = [UIFont systemFontOfSize:14];
    [bgView addSubview:phonelabel];
    
    UILabel *codelabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 62, 100, 25)];
    codelabel.text = @"请输入密码";
    codelabel.textColor = [UIColor blackColor];
    codelabel.textAlignment = NSTextAlignmentLeft;
    codelabel.font = [UIFont systemFontOfSize:14];
    [bgView addSubview:codelabel];
    
   UILabel * again = [[UILabel alloc]initWithFrame:CGRectMake(20, 112, 100, 25)];
    again.text = @"请确认密码";
    again.textColor = [UIColor blackColor];
    again.textAlignment = NSTextAlignmentLeft;
    again.font = [UIFont systemFontOfSize:14];
    [bgView addSubview:again];
    
   
    UIImageView *line1 = [self createImageViewFrame:CGRectMake(20, 50, bgView.frame.size.width-40, 1) imageName:nil color:[UIColor colorWithRed:180/255.0f green:180/255.0f blue:180/255.0f alpha:.3]];
    [bgView addSubview:line1];
    
    UIImageView *line2 = [self createImageViewFrame:CGRectMake(20, 100, bgView.frame.size.width-40, 1) imageName:nil color:[UIColor colorWithRed:180/255.0f green:180/255.0f blue:180/255.0f alpha:.3]];
    [bgView addSubview:line2];
    
    UIButton *landBtn = [self createButtonFrame:CGRectMake(10, bgView.frame.size.height+bgView.frame.origin.y+50,self.view.frame.size.width-20, 37) backImageName:nil title:@"下一步" titleColor:[UIColor whiteColor]  font:[UIFont systemFontOfSize:17] target:self action:@selector(next:)];
    
    landBtn.backgroundColor=[UIColor colorWithRed:248/255.0f green:144/255.0f blue:34/255.0f alpha:1];
    
    landBtn.layer.cornerRadius=5.0f;
    [self.view addSubview:landBtn];

}

-(void)next:(UIButton *)sender{
    
//    [LoginTool signUpWithname:phone.text password:code.text complete:^(BOOL isSuccess) {
//        if (isSuccess) {
//            zhuceTwoViewController *two = [zhuceTwoViewController new];
//            [self.navigationController pushViewController:two animated:YES];
//        }else{
//        
//        
//        }
//    }];
//    [LoginTool signUpOrLoginWithPhoneNumber:phone.text smsCode:code .text complete:^(User * _Nullable user, NSError * _Nullable err) {
//        if (!err) {
//            zhuceTwoViewController *two = [zhuceTwoViewController new];
//            [self.navigationController pushViewController:two animated:YES];
//        }else{
//            NSLog(@"%@",err);
//        }
//    }];
    
    
    zhuCeCompleteViewController *two = [zhuCeCompleteViewController new];
    [self.navigationController pushViewController:two animated:YES];
    
}

//-(void)getValidCode:(UIButton *)sender
//{
//    __block  int timeout = 10;
//    dispatch_queue_t queue =dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
//    dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), 1.0*NSEC_PER_SEC, 0);
//    if ([phone.text isEqualToString:@""]) {
//        return ;
//    }else if (phone.text.length <11)
//    {
//        return;
//    }
//    dispatch_source_set_event_handler(_timer, ^{
//        
//        if (timeout <= 0) {
//            dispatch_source_cancel(_timer);
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [yzButton setTitle:@"重新获取验证码" forState:UIControlStateNormal];
//                yzButton.userInteractionEnabled = YES;
//            });
//        }else{
//            int seconds = timeout % 60;
//            NSString *strTime = [NSString stringWithFormat:@"%.2d",seconds];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [UIView beginAnimations:nil context:nil];
//                [UIView setAnimationDuration:1];
//                [yzButton setTitle:[NSString stringWithFormat:@"%@秒后重新发送",strTime] forState:UIControlStateNormal];
//                [UIView commitAnimations];
//            });
//            timeout--;
//            
//        }
//    });
//    dispatch_resume(_timer);
//
//
//}

-(UIImageView *)createImageViewFrame:(CGRect)frame imageName:(NSString *)imageName color:(UIColor *)color
{
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:frame];
    
    if (imageName)
    {
        imageView.image=[UIImage imageNamed:imageName];
    }
    if (color)
    {
        imageView.backgroundColor=color;
    }
    
    return imageView;
}

-(UITextField *)createTextFielfFrame:(CGRect)frame font:(UIFont *)font placeholder:(NSString *)placeholder
{
    UITextField *textField=[[UITextField alloc]initWithFrame:frame];
    
    textField.font = font;
    
    textField.textColor = [UIColor grayColor];
    
    textField.borderStyle = UITextBorderStyleNone;
    
    textField.placeholder = placeholder;
    
    return textField;
}

-(UIButton *)createButtonFrame:(CGRect)frame backImageName:(NSString *)imageName title:(NSString *)title titleColor:(UIColor *)color font:(UIFont *)font target:(id)target action:(SEL)action
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    
    btn.frame = frame;
    
    if (imageName)
    {
        [btn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    
    if (font)
    {
        btn.titleLabel.font=font;
    }
    
    if (title)
    {
        [btn setTitle:title forState:UIControlStateNormal];
    }
    
    if (color)
    {
        [btn setTitleColor:color forState:UIControlStateNormal];
    }
    
    if (target&&action)
    {
        [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    }
    return btn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
