//
//  zhuCeCompleteViewController.m
//  MySport
//
//  Created by lanou3g on 16/3/4.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "zhuCeCompleteViewController.h"

@interface zhuCeCompleteViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIView *bgView;
    UITextField *username;
}

@property(nonatomic,strong)UIImageView *head;

@end

@implementation zhuCeCompleteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"注册完成";
    self.view.backgroundColor = [UIColor colorWithRed:240/255.0f green:240/255.0f blue:240/255.0f alpha:1];
    
    [self createTextFileds];
    [self creaHearVIiew];
    // Do any additional setup after loading the view.
}
-(void)createTextFileds{
    
    CGRect frame = [UIScreen mainScreen].bounds;
    bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 270, frame.size.width, 50)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    
    username  = [self createTextFielfFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 30) font:[UIFont systemFontOfSize:14] placehodel:@"请输入昵称"];
    username.textAlignment = NSTextAlignmentCenter;
    username.clearButtonMode = UITextFieldViewModeWhileEditing;
    [bgView addSubview:username];
    
    UIButton *landBtn = [self createButtonFrame:CGRectMake(10, bgView.frame.size.height + bgView.frame.origin.y + 30, self.view.frame.size.width-20, 37) backImageName:nil title:@"完成" titleColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:17] target:self action:@selector(landClick)];
    landBtn.backgroundColor = [UIColor colorWithRed:248/255.0f green:144/255.0f blue:34/255.0f alpha:1];
    landBtn.layer.cornerRadius = 5.0f;
    [self.view addSubview:landBtn];
    
    
}

-(void)creaHearVIiew{
    
    UIImageView *hearview = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 250)];
    //    [hearview setImage:[UIImage imageNamed:@"mycenter_bg.png"]];
    
    hearview.backgroundColor=[UIColor grayColor];
    [self.view addSubview:hearview];
    
    self.head=[[UIImageView alloc]initWithFrame:CGRectMake((self.view.frame.size.width-80)/2, 110, 80, 80)];
    //    [self.head setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    self.head.layer.cornerRadius = 40;
    self.head.layer.masksToBounds = YES;
    self.head.userInteractionEnabled = YES;
    self.head.backgroundColor=[UIColor whiteColor];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeHeadView1)];
    [self.head addGestureRecognizer:tap];
    //    [self.head addTarget:self action:@selector(changeHeadView1) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.head];
    
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width-80)/2, 180, 100, 80)];
    label.text = @"点击设置头像";
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:label];
}


-(void)landClick{
    
    
    
}

-(void)changeHeadView1{
    UIAlertController *alertController = [UIAlertController new];
    //添加Button
    [alertController addAction: [UIAlertAction actionWithTitle: @"拍照" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        //处理点击拍照
    }]];
    [alertController addAction: [UIAlertAction actionWithTitle: @"从相册选取" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        //处理点击从相册选取
        //创建图片选择器 1.初始化相册控制器
        UIImagePickerController *pickerVC = [[UIImagePickerController alloc]init];
        //设置代理
        pickerVC.delegate = self;
        // 允许编辑
        pickerVC.allowsEditing =YES;
        //选择图片源
        pickerVC.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //模态选择图片：打开图片选择器视图来选择图片
        [self presentViewController:pickerVC animated:YES completion:nil];
    }]];
    [alertController addAction: [UIAlertAction actionWithTitle: @"取消" style: UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController: alertController animated: YES completion: nil];
    
    
}
//选取完成时触发的方法  代理方法
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    
    self.head.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

-(UITextField *)createTextFielfFrame:(CGRect)frame font:(UIFont *)font placehodel:(NSString *)placeoder
{
    
    UITextField *textField = [[UITextField alloc]initWithFrame:frame];
    textField.font = font;
    textField.textColor = [UIColor grayColor];
    textField.borderStyle = UITextBorderStyleNone;
    textField.placeholder = placeoder;
    return textField;
    
}

-(UIImageView *)createImageViewFrame:(CGRect)frame imageName:(NSString *)imageName color:(UIColor *)color{
    
    UIImageView  *imageView = [[UIImageView alloc]initWithFrame:frame];
    if (imageName)
    {
        imageView.image = [UIImage imageNamed:imageName];
    }
    if (color)
    {
        imageView.backgroundColor = color;
    }
    return imageView;
    
}


-(UIButton *)createButtonFrame:(CGRect)frame backImageName:(NSString *)imageName title:(NSString *)title titleColor:(UIColor *)color font:(UIFont *)font target:(id)target action:(SEL)action{
    
    UIButton *btn = [ UIButton buttonWithType:UIButtonTypeCustom];
    
    btn.frame = frame;
    
    if (imageName)
    {
        [btn setBackgroundImage:[UIImage imageNamed:imageName] forState:(UIControlStateNormal)];
        
    }if (font)
    {
        btn.titleLabel.font = font;
    }
    if (title)
    {
        [btn setTitle:title forState:(UIControlStateNormal)];
    }
    if (target&&action)
    {
        [btn addTarget:target action:action forControlEvents:(UIControlEventTouchUpInside)];
    }
    return btn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
