//
//  AchieveViewTableViewCell.m
//  MySport
//
//  Created by lanou3g on 16/2/27.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "AchieveViewTableViewCell.h"
#import "Masonry.h"
#define kwidth self.contentView.bounds.size.width
#define kheight self.contentView.bounds.size.height

@implementation AchieveViewTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addviews];
    }return self;
}
-(void)addviews{
    //排名
    self.rankinglabel = [[UILabel alloc] init];
    [self.contentView addSubview:self.rankinglabel];
    [self.rankinglabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(20);
        make.bottom.equalTo(self.contentView).with.offset(-30);
        
    }];
    
    
    
    
    
    //头像
    self.image = [[UIImageView alloc] init];
    self.image.backgroundColor = [UIColor orangeColor];
    [self.contentView addSubview:self.image];
    self.image.frame = CGRectMake(self.contentView.frame.size.width * 0.2, self.contentView.frame.size.height * 0.4, self.contentView.frame.size.height*1, self.contentView.frame.size.height * 1);
    self.image.layer.masksToBounds = YES;
        self.image.layer.cornerRadius =  (self.contentView.frame.size.height*1)/2.0;//设置图片形状
    //姓名
    self.namelabel = [[UILabel alloc] init];
    [self.contentView addSubview:self.namelabel];
        self.namelabel.backgroundColor = [UIColor whiteColor];
    [self.namelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(self.contentView.frame.size.width * 0.4);
        make.bottom.equalTo(self.contentView).with.offset(-40);
    
    }];
    //性别
    self.sexlabel = [[UILabel alloc] init];
        self.sexlabel.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.sexlabel];
    [self.sexlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(self.contentView.frame.size.width * 0.4);
        make.bottom.equalTo(self.contentView).with.offset(-20);
    }];
    
    //步数
    self.numberlabel = [[UILabel alloc] init];
    self.numberlabel.font = [UIFont systemFontOfSize:25];//大小
        self.numberlabel.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.numberlabel];
    [self.numberlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(self.contentView.frame.size.width * 0.6);
        make.bottom.equalTo(self.contentView).with.offset(-30);
    }];
    //pk按钮
    self.Pkbut = [[UIButton alloc] init];
    self.Pkbut = [UIButton buttonWithType:UIButtonTypeSystem];
    self.Pkbut.backgroundColor = [UIColor whiteColor];
    self.Pkbut.layer.masksToBounds = YES;
    self.Pkbut.layer.cornerRadius = (self.contentView.frame.size.height * 0.5)/2.0;//设置圆角
    [self.contentView addSubview:self.Pkbut];
    [self.Pkbut setTitle:@"PK" forState:UIControlStateNormal];
    [self.Pkbut addTarget:self action:@selector(PKAtion:) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    
    //关注按钮
    self.attention = [[UIButton alloc] init];
    self.attention = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.attention.layer.masksToBounds = YES;
//    self.attention.layer.cornerRadius =  (kwidth * 0.1)/2.0;//设置圆角
    self.attention.backgroundColor = [UIColor whiteColor];
    [self.attention setImage:[UIImage imageNamed:@"heart_like_33.909090909091px_1187877_easyicon.net"] forState:UIControlStateNormal];
    self.attention.alpha = 0.5;
    [self.contentView addSubview:self.attention];
    
    [self.attention addTarget:self action:@selector(attAtion:) forControlEvents:(UIControlEventTouchUpInside)];
    
}
-(void)layoutSubviews{
    self.attention.frame = CGRectMake(kwidth * 0.9, kheight * 0.3, kwidth * 0.1, kheight * 0.35);
    self.Pkbut.frame = CGRectMake(kwidth * 0.75, kheight * 0.2, kheight * 0.6, kheight * 0.6);
}
//PK事件
-(void)PKAtion:(UIButton *)sender{
//    [self.Pkbut setTitle:@"PK" forState:UIButtonTypeSystem];
    
    
}
//关注事件
-(void)attAtion:(UIButton *)sender{
[self.attention setImage:[UIImage imageNamed:@"heart_like_34.200573065903px_1187665_easyicon.net"] forState:UIControlStateNormal];
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
