//
//  OutsideViewController.swift
//  MySport
//
//  Created by xirui on 16/3/2.
//  Copyright © 2016年 ccsobj. All rights reserved.
//
import UIKit
import Masonry

let mainBounds = UIScreen.mainScreen().bounds

class OutsideViewController: UIViewController ,CLLocationManagerDelegate, MAMapViewDelegate {
    
    let manager:CLLocationManager = CLLocationManager()
    let mapView:MAMapView = MAMapView()
    var functionOpen:Bool = true
    let functionV:FunctionView = FunctionView()
    let openFunc:UIButton = UIButton()
    //是否在跑步
    let running:Bool = true
    //路径数据
//    var polyline:MAMultiPolyline = MAMultiPolyline()
    var handleLocationData:HandleLocationData = HandleLocationData()

    override func viewDidAppear(animated: Bool) {
        
        self.mapView.zoomLevel = 18
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //添加地图视图和功能视图
        self.addViews()
        //添加返回按键
        self.addButton()
        //设置定位功能
        self.setLocation()
        //打开时设置为开启功能页面
        self.functionV.functionSet = functionOpen
        //开始更新位置
        self.manager.startUpdatingLocation()
        //添加按键功能
        self.addFunctionVButtonTarget()
        

    }
    
    func setLocation() -> Void{
        //设置代理
        self.manager.delegate = self
        self.mapView.delegate = self
        //显示位置
        self.mapView.showsUserLocation = true
        //缩放
//        self.mapView.zoomEnabled = true
        //开启定位
        self.manager.requestAlwaysAuthorization()
        self.manager.requestWhenInUseAuthorization()
        self.manager.startUpdatingLocation()
        self.mapView.userTrackingMode = MAUserTrackingMode.Follow
        

    }
    
    func addFunctionVButtonTarget(){
        self.functionV.lockButton.addTarget(self, action: ("lockScreen"), forControlEvents: UIControlEvents.TouchUpInside)
        self.functionV.pauseButton.addTarget(self, action: ("pauseRunning"), forControlEvents: UIControlEvents.TouchUpInside)
        self.functionV.endButton.addTarget(self, action: ("endRunning"), forControlEvents: UIControlEvents.TouchUpInside)
    }
        func lockScreen(){
            print("lockScreen")

        }
    
        func pauseRunning(){
            print("pauseRunning")
            if(self.functionV.pauseButton.titleLabel?.text == "暂停"){
                self.manager.stopUpdatingLocation()
                self.functionV.pauseButton.setTitle("开始", forState: UIControlState.Normal)
            }
            else if(self.functionV.pauseButton.titleLabel?.text == "开始"){
                self.manager.startUpdatingLocation()
                self.functionV.pauseButton.setTitle("暂停", forState: UIControlState.Normal)
            }
        }
    
        func endRunning(){
            print("endRunning")
        }

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.handleLocationData.handleData(self.manager.location)
        
        if(self.handleLocationData.polyline != nil){
            self.mapView.addOverlay(self.handleLocationData.polyline)
        }
        
//        print(self.initLocationData.polyline)
    }
    
    
    func mapView(mapView: MAMapView!, rendererForOverlay overlay: MAOverlay!) -> MAOverlayRenderer! {
        
        let polylineRenderer:MAMultiColoredPolylineRenderer = MAMultiColoredPolylineRenderer(multiPolyline: self.handleLocationData.polyline)
        polylineRenderer.lineWidth = 8
        polylineRenderer.strokeColors = self.handleLocationData.speedColors as [AnyObject]
        polylineRenderer.gradient = true
                
        return polylineRenderer;
    }
    
//-------------------- mark 后台运行 -------------------
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//-------------------- mark 以下均为页面布局 -------------------
    //添加视图
    func addViews()
    {
        self.mapView.frame = CGRectMake(0, 44, mainBounds.size.width, mainBounds.size.height*0.4 - 44)
        

        self.view.addSubview(self.mapView)
        
        self.view.addSubview(self.functionV)
    }
    
    //添加按键
    func addButton()
    {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "隐藏", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("backToHome"))
        
        //扩展功能页面按钮
        self.openFunc.frame = CGRectMake(self.mapView.frame.width - 50, self.mapView.frame.size.height - 50, 40, 40)
        self.openFunc.addTarget(self, action: Selector("openFunctionV"), forControlEvents: UIControlEvents.TouchUpInside)
        self.openFunc.backgroundColor = UIColor.orangeColor()
        self.mapView.addSubview(self.openFunc)
        
    }
    
    func backToHome(){
        
        self.navigationController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        
        })
    }
    
    //打开/关闭功能页面
    func openFunctionV(){
        
        print(self.functionOpen)
        

        if(self.functionOpen == true)
        {
            self.closeFunctionV()
            self.SetupdateViewFrame()
            self.functionV.functionSet = false
            self.functionOpen = false
        }
        else
        {
            self.openFunctionView()
            self.SetupdateViewFrame()
            self.functionV.functionSet = true
            self.functionOpen = true
        }
    }
    
    //缩小后的布局
    func closeFunctionV()
    {
        //设置关闭功能窗口下方剩余距离
        let closedHeight:CGFloat = 0.08
        //字体大小  由高度决定  高度 x 系数   整体修改字体大小可以改变系数   5 568  6 667
        let fontSize:CGFloat = UIScreen.mainScreen().bounds.height * 0.6

        
        self.functionV.mas_updateConstraints({ (make : MASConstraintMaker!) -> Void in
            make.top.setOffset(self.view.frame.height*(1-closedHeight))
            make.left.setOffset(0)
            make.width.height().setSizeOffset(CGSizeMake(self.view.frame.width, self.view.frame.height * closedHeight))
        })
        
        self.mapView.mas_updateConstraints({ (make : MASConstraintMaker!) -> Void in
            make.top.setOffset(44)
            make.left.setOffset(0)
            make.size.setSizeOffset(CGSizeMake(self.view.frame.width, self.view.frame.height*(1-closedHeight) - 44))
        })
        
        self.openFunc.mas_updateConstraints({ (make:MASConstraintMaker!) -> Void in
            make.top.setOffset(self.view.frame.height*(1-closedHeight) - 50 - 44)//导航栏高44
            make.left.setOffset(self.view.frame.width - 50)
            make.width.height().setSizeOffset(CGSizeMake(40,40))
            
        })
        
        self.functionV.firstLable.mas_updateConstraints({ (make:MASConstraintMaker!) -> Void in
            make.top.setOffset(0)
            make.left.setOffset(0)
            make.size.setSizeOffset(CGSizeMake(self.view.frame.width*0.5, self.view.frame.height * closedHeight))
            self.functionV.firstLable.font = UIFont.systemFontOfSize(closedHeight * fontSize)
            
        })
        
        self.functionV.firstMarkLable.mas_updateConstraints({ (make:MASConstraintMaker!) -> Void in
            //距离上的高度为 closedHeight - 自身高度
            make.top.setOffset(self.view.frame.height * closedHeight - self.view.frame.height*0.04)
            make.left.setOffset(self.view.frame.width*0.37)
            //frame大小根据屏幕大小*0.5 - 自身距离左边的距离  ⬆️
            make.size.setSizeOffset(CGSizeMake(self.view.frame.width*(0.5-0.37), self.view.frame.height*0.04))
        })
        self.functionV.secondLable.mas_updateConstraints({ (make:MASConstraintMaker!) -> Void in
            make.top.setOffset(0)
            make.left.setOffset(self.view.frame.width*0.5)
            make.size.setSizeOffset(CGSizeMake(self.view.frame.width*0.5, self.view.frame.height * closedHeight))
            self.functionV.secondLable.font = UIFont.systemFontOfSize(fontSize * closedHeight)
            
        })

    }
    
    //
    func openFunctionView()
    {
        //字体大小  由高度决定  高度 x 系数   整体修改字体大小可以改变系数   5 568  6 667
        let fontSize:CGFloat = UIScreen.mainScreen().bounds.height * 0.6
        
        self.functionV.mas_updateConstraints({ (make : MASConstraintMaker!) -> Void in
            make.left.setOffset(0)
            make.top.setOffset(self.view.frame.height*0.4)
            make.width.height().setSizeOffset(CGSizeMake(self.view.frame.width, self.view.frame.height*0.6))
        })
        
        self.mapView.mas_updateConstraints({ (make : MASConstraintMaker!) -> Void in
            make.left.setOffset(0)
            make.top.setOffset(44)
            make.size.setSizeOffset(CGSizeMake(self.view.frame.width, self.view.frame.height*0.4 - 44))
            
        })
        
        self.openFunc.mas_updateConstraints({ (make:MASConstraintMaker!) -> Void in
            make.top.setOffset(self.view.frame.height*0.4 - 50 - 44)//导航栏高44
            make.left.setOffset(self.view.frame.width - 50)
            make.width.height().setSizeOffset(CGSizeMake(40,40))
            
        })
        
        // funcV中两个按键是以funcV为参造物设置的布局 但是这里不能以funcV为参照物    以self.view作为参照物,  需要换算
        // funcV的高为全屏的0.6倍
        self.functionV.firstLable.mas_updateConstraints({ (make:MASConstraintMaker!) -> Void in
            make.top.setOffset(self.view.frame.height*0.08*0.6)
            make.left.setOffset(self.view.frame.width*0.15)
            make.size.setSizeOffset(CGSizeMake(self.view.frame.width*0.7, self.view.frame.height*0.16*0.6))
            self.functionV.firstLable.font = UIFont.systemFontOfSize(fontSize * 0.16)
        })
        
        
        self.functionV.firstMarkLable.mas_updateConstraints({ (make:MASConstraintMaker!) -> Void in
            make.top.setOffset(self.view.frame.height*0.24*0.6)
            make.left.setOffset(self.view.frame.width*0.45)
            make.size.setSizeOffset(CGSizeMake(self.view.frame.width*0.1, self.view.frame.height*0.06*0.6))
        })
        
        self.functionV.secondLable.mas_updateConstraints({ (make:MASConstraintMaker!) -> Void in
            make.top.setOffset(self.view.frame.height*0.32*0.6)
            make.left.setOffset(self.view.frame.width*0.3)
            make.size.setSizeOffset(CGSizeMake(self.view.frame.width*0.4, self.view.frame.height*0.08*0.6))
            self.functionV.secondLable.font = UIFont.systemFontOfSize(fontSize * 0.07)
        })
    }
    
    
    //更新布局设置
    func SetupdateViewFrame()
    {
        
        self.mapView.setNeedsUpdateConstraints()
        self.mapView.updateConstraintsIfNeeded()
        self.functionV.setNeedsUpdateConstraints()
        self.functionV.updateConstraintsIfNeeded()
        self.openFunc.setNeedsUpdateConstraints()
        self.openFunc.updateConstraintsIfNeeded()
        self.functionV.firstLable.setNeedsUpdateConstraints()
        self.functionV.firstLable.updateConstraintsIfNeeded()
        self.functionV.firstMarkLable.setNeedsUpdateConstraints()
        self.functionV.firstMarkLable.setNeedsUpdateConstraints()
        self.functionV.secondLable.setNeedsUpdateConstraints()
        self.functionV.secondLable.updateConstraintsIfNeeded()

        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.mapView.layoutIfNeeded()
        })
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.functionV.layoutIfNeeded()
        })
    }
    
//----------------   mark   ---------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
