//
//  HandleLocationData.h
//  MySport
//
//  Created by xirui on 16/3/8.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MAMapKit/MAMapKit.h>

@interface HandleLocationData : NSObject

@property (nonatomic,strong)NSMutableArray *drawArray;
@property (nonatomic,strong)NSMutableArray *dataArray;
@property (nonatomic,strong)NSMutableArray * speedColors;
@property (nonatomic,assign)CLLocationCoordinate2D * runningCoords;
@property (nonatomic,assign)NSUInteger count;
@property (nonatomic,strong)MAMultiPolyline * polyline;

- (void)handleData:(CLLocation*)locationData;



@end
