//
//  FunctionView.swift
//  MySport
//
//  Created by xirui on 16/3/2.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

class FunctionView: UIView {

    let openFunc:UIButton = UIButton()
    let firstLable:UILabel = UILabel()
    let firstMarkLable:UILabel = UILabel()
    let secondLable:UILabel = UILabel()
    let secondImageV:UIImageView = UIImageView()
    let secondMarkLable:UILabel = UILabel()
    let leftLable:UILabel = UILabel()
    let leftMarkLable:UILabel = UILabel()
    let middleLable:UILabel = UILabel()
    let middleMarkLable:UILabel = UILabel()
    let rightLable:UILabel = UILabel()
    let rightMarkLable:UILabel = UILabel()
    let lockButton:UIButton = UIButton()
    let pauseButton:UIButton = UIButton()
    let endButton:UIButton = UIButton(type: UIButtonType.Custom)
    var functionSet:Bool = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        print("View",self.functionSet)
        
        if(self.functionSet == true)
        {
            self.frame = CGRectMake(0, mainBounds.height*0.4, mainBounds.width, mainBounds.height*0.6)
        }
        
        self.backgroundColor = UIColor.init(red: 0.33, green: 0.33, blue: 0.33, alpha: 1)
        
        self.addViews()
        self.addButton()
        self.addLableText()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addViews(){
        
        //字体大小  由高度决定  高度 x 系数   整体修改字体大小可以改变系数   5 568  6 667
        let fontSize:CGFloat = UIScreen.mainScreen().bounds.height * 0.6
        print(fontSize)
        let selfFrame:CGSize = self.frame.size

//        self.firstLable.frame = CGRectMake(0,selfFrame.height*0.08, selfFrame.width, self.frame.size.height*0.16)
        self.firstLable.frame.size = CGSizeMake(selfFrame.width*0.7, self.frame.size.height*0.16)
        self.firstLable.center = CGPointMake(selfFrame.width * 0.5, self.frame.size.height*0.16)
        self.firstLable.textAlignment = NSTextAlignment.Center
        self.firstLable.font = UIFont.systemFontOfSize(0.18 * fontSize)
//        self.firstLable.backgroundColor = UIColor.cyanColor()
        self.firstLable.textColor = UIColor.whiteColor()
        self.addSubview(self.firstLable)

//        self.firstMarkLable.frame = CGRectMake(0,selfFrame.height*0.24, selfFrame.width, self.frame.size.height*0.08)
        self.firstMarkLable.frame.size = CGSizeMake(selfFrame.width*0.1, self.frame.size.height*0.08)
        self.firstMarkLable.center = CGPointMake(selfFrame.width * 0.5, self.frame.size.height*0.28)
        self.firstMarkLable.font = UIFont.systemFontOfSize(0.04 * fontSize)
        self.firstMarkLable.textAlignment = NSTextAlignment.Center
        self.firstMarkLable.textColor = UIColor.whiteColor()
//        self.firstMarkLable.backgroundColor = UIColor.blueColor()
        self.addSubview(self.firstMarkLable)
        
//        self.secondLable.frame = CGRectMake(0,selfFrame.height*0.32, selfFrame.width, selfFrame.height*0.08)
        self.secondLable.frame.size = CGSizeMake(selfFrame.width*0.4, self.frame.size.height*0.08)
        self.secondLable.center = CGPointMake(selfFrame.width * 0.5, self.frame.size.height*0.36)
        self.secondLable.font = UIFont.systemFontOfSize(0.07 * fontSize)
        self.secondLable.textAlignment = NSTextAlignment.Center
        self.secondLable.textColor = UIColor.whiteColor()
//        self.secondLable.backgroundColor = UIColor.redColor()
        self.addSubview(self.secondLable)
        //
        self.secondImageV.frame.size = CGSizeMake(selfFrame.height*0.06, selfFrame.height*0.06)
        self.secondImageV.center = CGPointMake(selfFrame.width*0.3,selfFrame.height*0.36)
//        self.secondImageV.backgroundColor = UIColor.yellowColor()
        self.addSubview(self.secondImageV)
        
        self.leftLable.frame = CGRectMake(selfFrame.width*0.08, selfFrame.height*0.5, selfFrame.width*0.28, selfFrame.height*0.1)
        self.leftLable.font = UIFont.systemFontOfSize(0.08 * fontSize)
        self.leftLable.textAlignment = NSTextAlignment.Center
        self.leftLable.textColor = UIColor.whiteColor()
//        self.leftLable.backgroundColor = UIColor.yellowColor()
        self.addSubview(self.leftLable)
        
        self.leftMarkLable.frame = CGRectMake(self.leftLable.frame.origin.x, CGRectGetMaxY(self.leftLable.frame), self.leftLable.frame.width, selfFrame.height*0.04)
        self.leftMarkLable.font = UIFont.systemFontOfSize(0.03 * fontSize)
        self.leftMarkLable.textAlignment = NSTextAlignment.Center
        self.leftMarkLable.textColor = UIColor.whiteColor()
//        self.leftMarkLable.backgroundColor = UIColor.whiteColor()
        self.addSubview(self.leftMarkLable)
        
        self.middleLable.frame = CGRectMake(self.leftLable.frame.origin.x + self.leftLable.frame.width, self.leftLable.frame.origin.y, self.leftLable.frame.width, self.leftLable.frame.height)
        self.middleLable.font = UIFont.systemFontOfSize(0.08 * fontSize)
        self.middleLable.textAlignment = NSTextAlignment.Center
        self.middleLable.textColor = UIColor.whiteColor()
//        self.middleLable.backgroundColor = UIColor.yellowColor()
        self.addSubview(self.middleLable)
        
        self.middleMarkLable.frame = CGRectMake(self.middleLable.frame.origin.x, CGRectGetMaxY(self.middleLable.frame), self.middleLable.frame.width, selfFrame.height*0.04)
        self.middleMarkLable.font = UIFont.systemFontOfSize(0.03 * fontSize)
        self.middleMarkLable.textAlignment = NSTextAlignment.Center
        self.middleMarkLable.textColor = UIColor.whiteColor()
//        self.middleMarkLable.backgroundColor = UIColor.whiteColor()
        self.addSubview(self.middleMarkLable)
        
        self.rightLable.frame = CGRectMake(self.middleLable.frame.origin.x + self.middleLable.frame.width, self.middleLable.frame.origin.y, self.middleLable.frame.width, self.middleLable.frame.height)
        self.rightLable.font = UIFont.systemFontOfSize(0.08 * fontSize)
        self.rightLable.textAlignment = NSTextAlignment.Center
        self.rightLable.textColor = UIColor.whiteColor()
//        self.rightLable.backgroundColor = UIColor.yellowColor()
        self.addSubview(self.rightLable)
        
        self.rightMarkLable.frame = CGRectMake(self.rightLable.frame.origin.x, CGRectGetMaxY(self.rightLable.frame), self.rightLable.frame.width, selfFrame.height*0.04)
        self.rightMarkLable.font = UIFont.systemFontOfSize(0.03 * fontSize)
        self.rightMarkLable.textAlignment = NSTextAlignment.Center
        self.rightMarkLable.textColor = UIColor.whiteColor()
//        self.rightMarkLable.backgroundColor = UIColor.whiteColor()
        self.addSubview(self.rightMarkLable)
    }
    
    //设置Lable内容
    func addLableText(){
        self.firstLable.text = "999.00"
        self.firstMarkLable.text = "公里"
        self.secondLable.text = "00:00:00"

        self.secondImageV.image = UIImage(named: "iconfont-shizhong")
        self.leftLable.text = "00:00"
        self.leftMarkLable.text = "海拔: 米"
//        self.middleLable.text = 00'00"
        self.middleMarkLable.text = "速度: 分钟/公里"
        self.rightLable.text = "0"
        self.rightMarkLable.text = "消耗卡路里: 大卡"
        
        
    }
    
    //添加按键
    func addButton(){
        
//三个按键及距离左右两边的距离为0.08倍的frame  左右半斤为0.1 中间半径为0.14
        self.lockButton.frame.size = CGSizeMake(self.frame.width*0.2, self.frame.width*0.2)
        self.lockButton.center = CGPointMake( self.frame.size.width*0.18 , self.frame.size.height*0.82)
        self.lockButton.layer.cornerRadius = self.frame.size.width*0.1
        self.lockButton.backgroundColor = UIColor.grayColor()
        self.lockButton.setBackgroundImage(UIImage(named: "iconpng"), forState: UIControlState.Normal)
//        self.lockButton.setTitle("锁屏", forState: UIControlState.Normal)
//        self.lockButton.addTarget(self, action: Selector("lockScreen"), forControlEvents: UIControlEvents.TouchDown)
        self.addSubview(self.lockButton)
        
        self.pauseButton.frame.size = CGSizeMake(self.frame.width*0.28, self.frame.width*0.28)
        self.pauseButton.center = CGPointMake( self.frame.size.width*0.5 , self.frame.size.height*0.82)
        self.pauseButton.layer.cornerRadius = self.frame.size.width*0.14
        self.pauseButton.backgroundColor = UIColor.init(red: 1, green: 0.8, blue: 0.3, alpha: 1)
        self.pauseButton.setTitle("暂停", forState: UIControlState.Normal)
        self.pauseButton.titleLabel?.font = UIFont.systemFontOfSize(24)
//        self.pauseButton.addTarget(self, action: Selector("pauseRunning"), forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(self.pauseButton)

        self.endButton.frame.size = CGSizeMake(self.frame.width*0.2, self.frame.width*0.2)
        self.endButton.center = CGPointMake( self.frame.size.width*0.82 , self.frame.size.height*0.82)
        self.endButton.layer.cornerRadius = self.frame.size.width*0.1
        self.endButton.backgroundColor = UIColor.redColor()
        self.endButton.setTitle("结束", forState: UIControlState.Normal)
//        self.endButton.addTarget(self, action: Selector("endRunning"), forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(self.endButton)
        
    }
    
//    func lockScreen(){
//        print("lockScreen")
//    }
//    
//    func pauseRunning(){
//        print("pauseRunning")
//    }
//    
//    func endRunning(){
//        print("endRunning")
//    }
    
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}




