//
//  HandleLocationData.m
//  MySport
//
//  Created by xirui on 16/3/8.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "HandleLocationData.h"

@implementation HandleLocationData

- (MAOverlayRenderer *)mapView:(MAMapView *)mapView rendererForOverlay:(id <MAOverlay>)overlay
{
    if (overlay == _polyline)
    {
        MAMultiColoredPolylineRenderer * polylineRenderer = [[MAMultiColoredPolylineRenderer alloc] initWithMultiPolyline:overlay];
        
        polylineRenderer.lineWidth = 8.f;
        polylineRenderer.strokeColors = _speedColors;
        polylineRenderer.gradient = YES;
        
        return polylineRenderer;
    }
    
    return nil;
}

- (UIColor *)getColorForSpeed:(float)speed
{
    const float lowSpeedTh = 2.f;
    const float highSpeedTh = 3.5f;
    const CGFloat warmHue = 0.02f; //偏暖色
    const CGFloat coldHue = 0.4f; //偏冷色
    
    float hue = coldHue - (speed - lowSpeedTh)*(coldHue - warmHue)/(highSpeedTh - lowSpeedTh);
    return [UIColor colorWithHue:hue saturation:1.f brightness:1.f alpha:1.f];
}

- (void)handleData:(CLLocation*)locationData
{
    _speedColors = [NSMutableArray array];
    _count = self.drawArray.count;
    _runningCoords = (CLLocationCoordinate2D *)malloc(_count * sizeof(CLLocationCoordinate2D));
    
    NSMutableArray * indexes = [NSMutableArray array];
    
    [self.dataArray addObject:locationData];
    
    if (self.drawArray.count <= 1) {
        [self.drawArray addObject:locationData];
    }
    else if(self.drawArray.count == 2)
    {
        [self.drawArray removeObjectAtIndex:0];
        [self.drawArray addObject:locationData];
    }
    
    if (self.drawArray)
    {
        for (int i = 0 ; i < _count; i++)
        {
            @autoreleasepool
            {
                CLLocation *data = self.drawArray[i];
                _runningCoords[i].latitude = data.coordinate.latitude;
                _runningCoords[i].longitude = data.coordinate.longitude;
                
                UIColor *speedColor = [self getColorForSpeed:data.speed];
                
                [_speedColors addObject:speedColor];
                
                [indexes addObject:@(i)];
            }
        }
    }
    
    _polyline = [MAMultiPolyline polylineWithCoordinates:_runningCoords count:_count drawStyleIndexes:indexes];
    NSLog(@"%@",_polyline);
    
}

//@property (nonatomic,strong)NSMutableArray * speedColors;
//@property (nonatomic,assign)CLLocationCoordinate2D * runningCoords;
//@property (nonatomic,assign)NSUInteger count;
//@property (nonatomic,strong)MAMultiPolyline * polyline;
//



-(NSMutableArray *)speedColors{
    if (_speedColors == nil) {
        _speedColors = [NSMutableArray array];
    }
    return _speedColors;
}

-(NSMutableArray *)dataArray{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


-(NSMutableArray *)drawArray{
    if (_drawArray == nil) {
        _drawArray = [NSMutableArray array];
    }
    return _drawArray;
}



@end
