//
//  LoginViewController.m
//  MySport
//
//  Created by lanou3g on 16/2/26.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginView.h"
#import "zhuceOneViewController.h"
#import "RetrievePasswordViewController.h"
#import "UMSocial.h"
#import <MySport-swift.h>

@interface LoginViewController ()

@property(nonatomic,strong)LoginView *loginView;

@end

@implementation LoginViewController

-(void)loadView{
    self.loginView = [[LoginView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.view = self.loginView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"讯跑";
    
    [self.loginView.dengluBtn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.registerBtn addTarget:self action:@selector(registerBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.findBtn addTarget:self action:@selector(findBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.weixinBtn addTarget:self action:@selector(WinxinAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.qqBtn addTarget:self action:@selector(QQAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.weiboBtn addTarget:self action:@selector(WeiboAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.travelBtn addTarget:self action:@selector(travelBtnClick:) forControlEvents:(UIControlEventTouchUpInside)];
}

-(void)WeiboAction:(UIButton *)sender{
    
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        //          获取微博用户名、uid、token等
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToSina];
            
            NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            
            [LoginTool loginWithName:snsAccount.userName uid:snsAccount.usid token:snsAccount.accessToken iconurl:snsAccount.iconURL];
            AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            delegate.window.rootViewController = [[TabBarController alloc]init];
            
            
        }});
    
}

-(void)QQAction:(UIButton *)sender{
    
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQQ];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        //          获取微博用户名、uid、token等
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToQQ];
            
            NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            
            [LoginTool loginWithName:snsAccount.userName uid:snsAccount.usid token:snsAccount.accessToken iconurl:snsAccount.iconURL];
            AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            delegate.window.rootViewController = [[TabBarController alloc]init];
        }});
    
}
-(void)WinxinAction:(UIButton *)sender{
    
    
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        //          获取微博用户名、uid、token等
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToWechatSession];
            
            NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            
            [LoginTool loginWithName:snsAccount.userName uid:snsAccount.usid token:snsAccount.accessToken iconurl:snsAccount.iconURL];
            AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            delegate.window.rootViewController = [[TabBarController alloc]init];
            
            
        }});
    
}
-(BOOL)isDirectShareInIconActionSheet{
    return NO;
}
-(void)registerBtn:(UIButton *)sender{
    
    zhuceOneViewController *zhuce = [zhuceOneViewController new];
    
    [self presentViewController:[[UINavigationController alloc]initWithRootViewController:zhuce] animated:YES completion:^{}];
    
}
-(void)findBtnAction:(UIButton *)sender{
    
    RetrievePasswordViewController *RP = [RetrievePasswordViewController new];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:RP];
    [self presentViewController:nav animated:YES completion:^{}];
}

- (void)travelBtnClick:(UIButton *)sender{
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.window.rootViewController = [[TabBarController alloc]init];
}

- (void)login
{
    
//    [LoginTool signUpWithname:self.loginView.IDField.text password:self.loginView.passwordField.text];
    [LoginTool login:self.loginView.IDField.text password:self.loginView.passwordField.text complete:^(User * _Nullable user, NSError * _Nullable err) {
        if (err) {
            NSLog(@"%@",err);
        }else{
            AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            delegate.window.rootViewController = [[TabBarController alloc]init];
        }
    }];

}

@end
