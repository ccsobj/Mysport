//
//  User.h
//  MySport
//
//  Created by lanou3g on 16/2/27.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface User : NSObject

@property (nonatomic,copy)NSString *username;
@property (nonatomic,copy)NSString *password;
@property (nonatomic,copy)NSString *email;
@property (nonatomic,copy)NSString *mobilePhoneNumber;
@property (nonatomic,copy)NSString *icon_url;
@property (nonatomic,copy)UIImage *icon;


+ (instancetype)userWithname:(NSString *)name password:(NSString *)password email:(NSString *)email mobilePhoneNumber:(NSString *)mobilePhoneNumber;

- (void)setIcon:(UIImage *)icon;


@end
