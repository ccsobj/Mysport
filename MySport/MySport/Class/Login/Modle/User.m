//
//  User.m
//  MySport
//
//  Created by lanou3g on 16/2/27.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "User.h"
#import "NSString+Hash.h"


@implementation User

+ (instancetype)userWithname:(NSString *)name password:(NSString *)password email:(NSString *)email mobilePhoneNumber:(NSString *)mobilePhoneNumber
{
    User *user = [[User alloc]init];
    user.username = name;
    user.password = password;
    user.email = email;
    user.mobilePhoneNumber = mobilePhoneNumber;
    return user;
}


@end
