//
//  PulmonaryTableViewCell.m
//  MySport
//
//  Created by lanou3g on 16/3/8.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "PulmonaryTableViewCell.h"

@interface PulmonaryTableViewCell ()<AVAudioRecorderDelegate>
@property (nonatomic ,strong)AVAudioRecorder *recorder;
@property (nonatomic ,strong)NSTimer *timer;
@property (nonatomic ,strong)NSURL *urlPlay;
@property (nonatomic,assign,getter=isBeging)BOOL beging;
@end




@implementation PulmonaryTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addviews];
        [self audio];
    }return self;
}



-(void)addviews{
    
    self.pulmonary =[[UILabel alloc] init];
    self.pulmonary.backgroundColor =[ UIColor brownColor];
    [self.contentView addSubview:self.pulmonary];
    self.testbut =[[UIButton alloc] init];
    self.testbut.backgroundColor = [UIColor brownColor];
    [self.testbut setTitle:@"开始测试!" forState:UIControlStateNormal];
    [self.testbut addTarget:self action:@selector(btnDown:) forControlEvents:UIControlEventTouchDown];
    [self.testbut addTarget:self action:@selector(btnDragUp:) forControlEvents:UIControlEventTouchDragExit];
    [self.contentView addSubview:self.testbut];
    self.grade = [[UILabel alloc] init];
    self.grade.backgroundColor = [UIColor brownColor];
    [self.contentView addSubview:self.grade];
}
//录音设置
-(void)audio{
    //录音设置
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc]init] ;
    //设置录音格式  AVFormatIDKey==kAudioFormatLinearPCM
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    //设置录音采样率(Hz) 如：AVSampleRateKey==8000/44100/96000（影响音频的质量）
    [recordSetting setValue:[NSNumber numberWithFloat:44100] forKey:AVSampleRateKey];
    //录音通道数  1 或 2
    [recordSetting setValue:[NSNumber numberWithInt:1] forKey:AVNumberOfChannelsKey];
    //线性采样位数  8、16、24、32
    [recordSetting setValue:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    //录音的质量
    [recordSetting setValue:[NSNumber numberWithInt:AVAudioQualityHigh] forKey:AVEncoderAudioQualityKey];
    
    NSString *strUrl = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/lll.aac", strUrl]];
    _urlPlay = url;
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *setCategoryError = nil;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&setCategoryError];
    
    if(setCategoryError){
        //        NSLog(@"%@", [setCategoryError description]);
    }
    NSError *error;
    //初始化
    _recorder = [[AVAudioRecorder alloc]initWithURL:url settings:recordSetting error:&error];
    //开启音量检测
    _recorder.meteringEnabled = YES;
    _recorder.delegate = self;
}
-(void)btnDown:(UIButton *)sender{
    NSLog(@"sdfadfasdfasd");
    if ([_recorder prepareToRecord]) {
        //开始录音
        [_recorder record];
    }
    
   _timer = [NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(detectionVoice) userInfo:nil repeats:YES];

}
-(void)btnDragUp:(UIButton *)sender{
    [_recorder deleteRecording];
    [_recorder stop];
    [self.timer invalidate];
    NSLog(@"取消发送");
}
-(void)detectionVoice{
    [_recorder updateMeters];//刷新音量数据
    //获取音量的平均值  [recorder averagePowerForChannel:0];
    //音量的最大值  [recorder peakPowerForChannel:0];
    double cTime = _recorder.currentTime;
    double lowPassResults = pow(10, (0.05 * [_recorder peakPowerForChannel:0]));
    NSLog(@"+++++++++++++++++%lf",lowPassResults);
    
    if (lowPassResults > 0.5) {
        self.beging = YES;
    }
    
    if (lowPassResults<0.2&& cTime > 0.4) {
        self.beging = YES;
        if (cTime > 2) {//如果录制时间<2 不发送
            
        }else {
            //删除记录的文件
            [_recorder deleteRecording];
        }
        [_recorder stop];
        [self.timer invalidate];
    }
    
    
    if (_recorder.currentTime > 0) {
        self.grade.text = [NSString stringWithFormat:@"%.2f",_recorder.currentTime * 260];
    }
    
}
-(void)layoutSubviews{
    self.pulmonary.frame = CGRectMake(self.contentView.frame.size.width * 0.1, self.contentView.frame.size.height * 0.3, self.contentView.frame.size.width * 0.15, self.contentView.frame.size.height * 0.4);
    self.testbut.frame = CGRectMake(self.contentView.frame.size.width * 0.37, self.contentView.frame.size.height * 0.2, self.contentView.frame.size.height * 0.6, self.contentView.frame.size.height * 0.6);
    self.testbut.layer.masksToBounds = YES;
    self.testbut.layer.cornerRadius =  (self.contentView.frame.size.height * 0.6)/2.0;
    
    
    self.grade.frame = CGRectMake(self.contentView.frame.size.width * 0.7, self.contentView.frame.size.height * 0.2, self.contentView.frame.size.height * 0.7, self.contentView.frame.size.height * 0.6);
    
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
