//
//  HomeTableViewCell.h
//  MySport
//
//  Created by lanou3g on 16/3/8.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTableViewCell : UITableViewCell

//提示
@property (nonatomic ,strong)UILabel *stepl;
@property (nonatomic ,strong)UILabel *distancel;
@property (nonatomic ,strong)UILabel *accumlatel;

@property (nonatomic ,strong)UILabel *step;//步数
@property (nonatomic ,strong)UILabel *distance;//距离
@property (nonatomic ,strong)UILabel *accumulate;//累计时间

@end
