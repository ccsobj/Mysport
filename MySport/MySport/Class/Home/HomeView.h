//
//  HomeView.h
//  MySport
//
//  Created by lanou3g on 16/3/8.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeView : UIView
@property (nonatomic ,strong)UIImageView *image;//地点图片
@property (nonatomic,strong)UILabel *site;//地点
@property (nonatomic,strong)UILabel *weather;//天气
@property (nonatomic,strong)UILabel *temperature;//温度
@property (nonatomic ,strong)UILabel *days;//时间
@property (nonatomic ,strong)UILabel *weekend;//周几
@property (nonatomic ,strong)UIImageView *weatherimage;//天气图片
@end
