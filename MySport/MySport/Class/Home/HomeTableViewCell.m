//
//  HomeTableViewCell.m
//  MySport
//
//  Created by lanou3g on 16/3/8.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "HomeTableViewCell.h"

@implementation HomeTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addviews];
    }return self;
}
-(void)addviews{
    //提示
    self.stepl = [[UILabel alloc] init];
    self.stepl.backgroundColor = [UIColor cyanColor];
//    self.stepl.text = @"步数";
    self.stepl.textAlignment = 1;
    self.stepl.frame = CGRectMake(self.contentView.frame.size.width * 0.1, self.contentView.frame.size.height * 2, self.contentView.frame.size.width * 0.2, self.contentView.frame.size.height * 1);
    [self addSubview:self.stepl];
    
    self.distancel = [[UILabel alloc] init];
    self.distancel.text = @"距离";
    self.distancel.textAlignment = 1;
    self.distancel.backgroundColor = [UIColor cyanColor];
    self.distancel.frame = CGRectMake(self.contentView.frame.size.width * 0.4, self.frame.size.height * 0.3, self.frame.size.width * 0.2, self.frame.size.height * 0.15);
    [self addSubview:self.distancel];

    self.accumlatel = [[UILabel alloc] init];
    self.accumlatel.textAlignment = 1;
    self.accumlatel.backgroundColor = [UIColor cyanColor];
    self.accumlatel.frame = CGRectMake(self.contentView.frame.size.width * 0.7, self.frame.size.height * 0.3, self.frame.size.width * 0.2, self.frame.size.height * 0.15);
    self.accumlatel.text = @"时间";
    [self addSubview:self.accumlatel];
    
    //步数
    self.step = [[UILabel alloc] init];
    self.step.backgroundColor = [UIColor blueColor];
    self.step.textAlignment = 1;
    self.step.frame = CGRectMake(self.frame.size.width * 0.1,self.frame.size.height * 0.32 , self.frame.size.width * 0.2, self.frame.size.height * 0.05);
    [self addSubview:self.step];
    //距离
    self.distance = [[UILabel alloc] init];
    self.distance.textAlignment = 1;
    self.distance.backgroundColor = [UIColor blueColor];
    self.distance.frame = CGRectMake(self.frame.size.width * 0.4, self.frame.size.height * 0.32, self.frame.size.width * 0.2, self.frame.size.height * 0.05);
    [self addSubview:self.distance];
    //时间
    self.accumulate = [[UILabel alloc] init];
    self.accumulate.textAlignment = 1;
    self.accumulate.backgroundColor = [UIColor blueColor];
    self.accumulate.frame = CGRectMake(self.frame.size.width * 0.7, self.frame.size.height * 0.32, self.frame.size.width * 0.2, self.frame.size.height * 0.05);
    [self addSubview:self.accumulate];
}

-(void)layoutSubviews{
    self.stepl.frame = CGRectMake(self.contentView.frame.size.width * 0.1, self.contentView.frame.size.height *0.7, self.contentView.frame.size.width * 0.2, self.contentView.frame.size.height * 0.3);

     self.distancel.frame = CGRectMake(self.contentView.frame.size.width * 0.4, self.contentView.frame.size.height *0.7, self.contentView.frame.size.width * 0.2, self.contentView.frame.size.height * 0.3);
     self.accumlatel.frame = CGRectMake(self.contentView.frame.size.width * 0.7, self.contentView.frame.size.height *0.7, self.contentView.frame.size.width * 0.2, self.contentView.frame.size.height * 0.3);
    
    self.step.frame = CGRectMake(self.contentView.frame.size.width * 0.1, self.contentView.frame.size.height *0.2, self.contentView.frame.size.width * 0.2, self.contentView.frame.size.height * 0.3);
    self.distance.frame = CGRectMake(self.contentView.frame.size.width * 0.4, self.contentView.frame.size.height *0.2, self.contentView.frame.size.width * 0.2, self.contentView.frame.size.height * 0.3);
    
    self.accumulate.frame = CGRectMake(self.contentView.frame.size.width * 0.7, self.contentView.frame.size.height *0.2, self.contentView.frame.size.width * 0.2, self.contentView.frame.size.height * 0.3);
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
