//
//  HomeModel.h
//  MySport
//
//  Created by lanou3g on 16/3/5.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeModel : NSObject
@property (nonatomic ,strong)NSString *citynm;//地点
@property (nonatomic ,strong)NSString *weather;//天气
@property (nonatomic ,strong)NSString *temperature;//温度
@property (nonatomic ,strong)NSString *days;//当前时期
@property (nonatomic ,strong)NSString *week;//周几
@property (nonatomic ,strong)NSString *weather_icon;//天气图片

@end
