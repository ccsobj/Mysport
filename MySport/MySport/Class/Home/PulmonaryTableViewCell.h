//
//  PulmonaryTableViewCell.h
//  MySport
//
//  Created by lanou3g on 16/3/8.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface PulmonaryTableViewCell : UITableViewCell
@property (nonatomic ,strong)UILabel *pulmonary;//肺活量提示
@property (nonatomic ,strong)UIButton *testbut;//测试按钮
@property (nonatomic ,strong)UILabel *grade;//分数显示
@end
