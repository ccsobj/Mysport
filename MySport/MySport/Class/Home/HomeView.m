//
//  HomeView.m
//  MySport
//
//  Created by lanou3g on 16/3/8.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "HomeView.h"
#import "HomeModel.h"
#import "UIImageView+WebCache.h"

#define weatherurl @"http://api.k780.com:88/?app=weather.today&weaid=1&&appkey=10003&sign=b59bc3ef6191eb9f747dd4e83c99f2a4&format=json"
@implementation HomeView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
        [self addviews];
        [self allviews];
    }
    return self;
}
-(void)addviews{
    //地点图
    self.image = [[UIImageView alloc] init];
    self.image.backgroundColor = [UIColor orangeColor];
    NSLog(@"%f",self.frame.size.width * 0.3);
    self.image.frame = CGRectMake(0,0,self.frame.size.height * 0.15,self.frame.size.height * 0.17);
    self.image.image = [UIImage imageNamed:@"icon_spot_blue"];
    [self addSubview:self.image];
    
    //地点
    self.site =[[UILabel alloc] init];
    self.site.backgroundColor = [UIColor orangeColor];
    self.site.frame = CGRectMake(self.image.frame.size.width, 0, self.frame.size.height * 0.3, self.frame.size.height * 0.17);
    [self addSubview:self.site];
    //天气图片
    self.weatherimage = [[UIImageView alloc] init];
    self.weatherimage.backgroundColor = [UIColor orangeColor];
    self.weatherimage.frame = CGRectMake(self.frame.size.width * 0.1,self.frame.size.height * 0.3,self.frame.size.width * 0.1,self.frame.size.height * 0.2);
    [self addSubview:self.weatherimage];
    //天气
    self.weather = [[UILabel alloc] init];
    self.weather.backgroundColor = [UIColor orangeColor];
    self.weather.frame = CGRectMake(self.frame.size.width * 0.25,self.frame.size.height * 0.3,self.frame.size.width * 0.2,self.frame.size.height * 0.2);
    [self addSubview:self.weather];
    //温度
    self.temperature = [[UILabel alloc] init];
    self.temperature.backgroundColor =[UIColor orangeColor];
    self.temperature.frame = CGRectMake(self.frame.size.width * 0.5, self.frame.size.height * 0.3, self.frame.size.width * 0.3, self.frame.size.height * 0.2);
    [self addSubview:self.temperature];
    //时间
    self.days = [[UILabel alloc] init];
    self.days.backgroundColor =[UIColor orangeColor];
    self.days.frame = CGRectMake(0, self.frame.size.height * 0.7, self.frame.size.width * 0.5, self.frame.size.height * 0.2);
    [self addSubview:self.days];
    //周几
    self.weekend = [[UILabel alloc] init];
    self.weekend.backgroundColor = [UIColor orangeColor];
    self.weekend.frame = CGRectMake(self.frame.size.width * 0.5, self.frame.size.height * 0.7, self.frame.size.width * 0.5, self.frame.size.height * 0.2);
    [self addSubview:self.weekend];
   
    
    
}
-(void)allviews{
    NSURL *url = [NSURL URLWithString:weatherurl];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            HomeModel *model = [[HomeModel alloc] init];
            [model setValuesForKeysWithDictionary:dic[@"result"]];
            self.weather.text = model.weather;
            self.site.text = model.citynm;
            self.temperature.text = model.temperature;
            self.days.text = model.days;
            self.weekend.text = model.week;
            [self.weatherimage sd_setImageWithURL:[NSURL URLWithString:model.weather_icon] placeholderImage:[UIImage imageNamed:@"1"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            }];
            
        });
        
    }];
    
    [dataTask resume];
    
}

@end
