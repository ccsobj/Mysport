//
//  HomeTableViewController.m
//  MySport
//
//  Created by lanou3g on 16/3/8.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "HomeTableViewController.h"
#import "HomeView.h"
#import "HomeCellModel.h"
#import "HomeTableViewCell.h"
#import "PulmonaryTableViewCell.h"
#import "BodyTableViewCell.h"
@interface HomeTableViewController ()
@property (nonatomic ,strong)HomeView *home;
@property (nonatomic ,strong)NSMutableArray *dataArray;
@end

@implementation HomeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //注册
    [self.tableView registerClass:[HomeTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableView registerClass:[PulmonaryTableViewCell class] forCellReuseIdentifier:@"cellID"];
    [self.tableView registerClass:[BodyTableViewCell class] forCellReuseIdentifier:@"cellbod"];
    //设置头视图
    self.tableView.separatorStyle = NO;
    self.automaticallyAdjustsScrollViewInsets = YES;
    self.navigationController.navigationBar.translucent = YES;
    self.home = [[HomeView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height  * 0.2)];
    self.home.backgroundColor = [UIColor cyanColor];
    self.tableView.tableHeaderView = self.home;
    
    [self addviews];
}
-(void)addviews{
   
    self.dataArray = [[NSMutableArray alloc] init];
    
    NSMutableArray *arr = [NSMutableArray array];
    NSMutableArray *arr2 = [NSMutableArray array];
    NSMutableArray *arr3 = [NSMutableArray array];
    
    
    HomeCellModel *homemodel = [[HomeCellModel alloc] init];
    homemodel.type = @"cell";
    
    HomeCellModel *pumodel = [[HomeCellModel alloc] init];
    pumodel.type = @"cellID";
    
    HomeCellModel *Bodymodel = [[HomeCellModel alloc] init];
    Bodymodel.type = @"cellBO";
    
    
    [arr addObject:homemodel];
    [arr2 addObject:pumodel];
    [arr3 addObject:Bodymodel];
    
    [self.dataArray addObject:arr];
    [self.dataArray addObject:arr2];
    [self.dataArray addObject:arr3];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [self.dataArray[section]count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    HomeCellModel *model = self.dataArray[indexPath.section][indexPath.row];
    if ([model.type isEqualToString:@"cell"]) {
        return 130;
    }else if ([model.type isEqualToString:@"cellID"]){
        return 150;
    }else if ([model.type isEqualToString:@"cellBO"]){
        return 100;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeCellModel *model = self.dataArray[indexPath.section][indexPath.row];
    if ([model.type isEqualToString:@"cell"]) {
         HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.stepl.text = @"步数";
        return cell;
    }else if ([model.type isEqualToString:@"cellID"]){
        PulmonaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
        cell.pulmonary.text = @"肺活量:";
        return cell;
    }else if ([model.type isEqualToString:@"cellBO"]){
        BodyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellbod"];
        cell.heightl.text = @"身高:";
        cell.weightl.text = @"体重:";
        cell.bloodl.text = @"血型:";
        return cell;
    }
     
    return nil;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
