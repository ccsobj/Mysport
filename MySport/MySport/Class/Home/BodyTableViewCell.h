//
//  BodyTableViewCell.h
//  MySport
//
//  Created by lanou3g on 16/3/8.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BodyTableViewCell : UITableViewCell
//提示
@property (nonatomic ,strong)UILabel *heightl;
@property (nonatomic ,strong)UILabel *weightl;
@property (nonatomic ,strong)UILabel *bloodl;

//
@property (nonatomic ,strong)UILabel *height;//身高
@property (nonatomic ,strong)UILabel *weight;//体重
@property (nonatomic ,strong)UILabel *blood;//血型

@end
