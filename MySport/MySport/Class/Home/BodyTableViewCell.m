//
//  BodyTableViewCell.m
//  MySport
//
//  Created by lanou3g on 16/3/8.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "BodyTableViewCell.h"

@implementation BodyTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addviews];
    }return self;
}
-(void)addviews{
//    @property (nonatomic ,strong)UILabel *heightl;
//    @property (nonatomic ,strong)UILabel *weightl;
//    @property (nonatomic ,strong)UILabel *bloodl;
//    
//    //
//    @property (nonatomic ,strong)UILabel *height;//身高
//    @property (nonatomic ,strong)UILabel *weight;//体重
//    @property (nonatomic ,strong)UILabel *blood;//血型
    self.heightl = [[UILabel alloc] init];
    self.heightl.backgroundColor = [UIColor orangeColor];
    [self.contentView addSubview:self.heightl];
    
    self.height = [[UILabel alloc] init];
    self.height.backgroundColor = [UIColor orangeColor];
    [self.contentView addSubview:self.height];
    
    self.weightl = [[UILabel alloc] init];
    self.weightl.backgroundColor = [UIColor orangeColor];
    [self.contentView addSubview:self.weightl];
    
    self.weight = [[UILabel alloc] init];
    self.weight.backgroundColor = [UIColor orangeColor];
    [self.contentView addSubview:self.weight];
    
    self.bloodl = [[UILabel alloc] init];
    self.bloodl.backgroundColor = [UIColor orangeColor];
    [self.contentView addSubview:self.bloodl];
    
    self.blood = [[UILabel alloc] init];
    self.blood.backgroundColor = [UIColor orangeColor];
    [self.contentView addSubview:self.blood];

}
-(void)layoutSubviews{
    self.heightl.frame = CGRectMake(self.contentView.frame.size.width * 0.02, self.contentView.frame.size.height * 0.3, self.contentView.frame.size.width * 0.1, self.contentView.frame.size.height * 0.4);
    self.height.frame = CGRectMake(self.contentView.frame.size.width * 0.12, self.contentView.frame.size.height * 0.3, self.contentView.frame.size.width * 0.15, self.contentView.frame.size.height * 0.4);
    self.weightl.frame = CGRectMake(self.contentView.frame.size.width * 0.35, self.contentView.frame.size.height * 0.3, self.contentView.frame.size.width * 0.1, self.contentView.frame.size.height * 0.4);
    self.weight.frame = CGRectMake(self.contentView.frame.size.width * 0.45, self.contentView.frame.size.height * 0.3, self.contentView.frame.size.width * 0.15, self.contentView.frame.size.height * 0.4);
    
    self.bloodl.frame = CGRectMake(self.contentView.frame.size.width * 0.68, self.contentView.frame.size.height * 0.3, self.contentView.frame.size.width * 0.1, self.contentView.frame.size.height * 0.4);
    
    self.blood.frame = CGRectMake(self.contentView.frame.size.width * 0.78, self.contentView.frame.size.height * 0.3, self.contentView.frame.size.width * 0.2, self.contentView.frame.size.height * 0.4);
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
