//
//  HttpTool.swift
//  MySport
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

import Alamofire

class HttpTool:NSObject{
   
    typealias ArrayResultslosure  = ([[String:AnyObject]]?)->()
    
   class func safeAsynTask(closure:()->()){
        if NSThread.currentThread().isMainThread {
            closure()
        }
        dispatch_async(dispatch_get_main_queue()){
            closure()
        }
    }
    
    
    class func getWithParam(param:ParamProtocol,complete:ArrayResultslosure){
        
     getWithURL(param.URLString,param:param.paramDic){
            if let json = $0{
                let arr = param.dictionaryArray(json)
                complete(arr)
            }
        }
    }
    
    class func getWithURL(url:String,param:[String:AnyObject]?,complete:(AnyObject?)->Void){
        
        Alamofire.request(.GET,url, parameters:param).responseJSON {result in
            
            safeAsynTask(){
                complete(result.result.value)
            }
        }
    }
}
