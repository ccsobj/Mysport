//
//  ClassMessage.swift
//  MySport
//
//  Created by lanou3g on 16/3/4.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

struct ClassMessage1 {
    //        类名
    let name : String
    //      int类型的所有属性
    var ints : [String]?
    //      string类型的所有属性
    var texts : [String]?
    //        double类型的所有属性
    var doubles : [String]?
    
    var uniqueText: [String]?
    
    
    init(aclass:AnyClass){
        name = NSStringFromClass(aclass).removeNamespace()
        if let obj = (name.object() as? NSObject){
            (ints,doubles,texts) = obj.keysTuple()
        }
    }
    
    func nameWithClass(){
        
    }
    
    
    mutating func list()->String{
        let ks = keys()
        let str  = ks.reduce(""){$0 + "," + $1}
        let str1 = "(" + str.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString:",")) + ")"
        var str2 = " values ("
        for  _ in 0..<ks.count{
            str2 += "?,"
        }
        str2 = str2.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString:","))
        return str1 + str2 + ");"
    }
    
    mutating func keys()->[String]{
        var list = [String]()
        if let uni = uniqueText{
            list += uni
        }
        if let int = ints{
            list += int
        }
        if let text = texts{
            list += text
        }
        if let double = doubles{
            list += double
        }
        return list
    }
    
    mutating func instance()->NSObject?{
        if let clazz = NSClassFromString(name) as? NSObject.Type{
            return clazz.init()
        }else if let clazz = NSClassFromString(name.addNamespace()) as? NSObject.Type{
            return clazz.init()
        }
        return nil
    }
}
