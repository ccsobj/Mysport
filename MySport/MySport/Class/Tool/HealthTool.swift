//
//  HealthTool.swift
//  MySport
//
//  Created by lanou3g on 16/2/27.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

import HealthKit

import Async


class HealthTool: NSObject {
    
    private static let healthStrore = HKHealthStore()
    
    typealias DoubleResultClosure = (Double?)->Void
    typealias HealthInfoResultClosure = (HealthInfo)->Void
    private static let _healthTool = HealthTool()
    private override init() {}
    class func shareHealthTool()->HealthTool{
        return _healthTool
    }
    
    static private var isAuthorized : Bool = false
    
    static var available : Bool{
        return HKHealthStore.isHealthDataAvailable()
    }
    
    private static let healthkitTypeToRead  = Set([HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierDateOfBirth)!,
        HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBloodType)!,
        HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBiologicalSex)!,HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!,
        HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!,
        HKObjectType.workoutType(),
        HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMassIndex)!,
        HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!,
        HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!,
        HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierForcedVitalCapacity)!])
    private static let healthkitTypeToWrite = Set([HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!,HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!,HKObjectType.workoutType(),HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMassIndex)!,HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!,HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!,
        HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierForcedVitalCapacity)!])
    
//    获取Health的授权
    class func authorizeHealthKit(completion:((isSuccess:Bool,error:NSError!)->Void)!){
        healthStrore.requestAuthorizationToShareTypes(healthkitTypeToWrite, readTypes:healthkitTypeToRead){
            completion(isSuccess: $0,error: $1)
            if $0{
                isAuthorized = true
            }else{
                print($1)
            }
        }
    }
    
//    从Health获取数据
    class func getInfoFromHealth(complete:HealthInfoResultClosure){
        if !isAuthorized{
            authorizeHealthKit(){_,_ in}
        }
        let healthInfo = HealthInfo()
        healthInfo.sex = try? healthStrore.biologicalSex().biologicalSex
        healthInfo.birthDay = try? healthStrore.dateOfBirth()
        healthInfo.bloodType = try? healthStrore.bloodType().bloodType
        getHeight(){result in
            healthInfo.heightSample = result
            getWeight(){result in
                healthInfo.weightSample = result
                    getVital({ (sample) -> Void in
                      healthInfo.vitalCapacitySample = sample
                        complete(healthInfo)
                     })
                }
            }
        }
    
    private class func getVital(complete:(HKSample?)->Void){
        let type = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierForcedVitalCapacity)
        self.readMostRecentSample(type!){sample,_ in
            complete(sample)
        }
    }
    
    private class func getWeight(complete:(HKSample?)->Void){
        let type = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
        self.readMostRecentSample(type!){sample,_ in
            complete(sample)
        }
    }
    
    private class func getHeight(complete:(HKSample?)->Void){
        let type = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
        self.readMostRecentSample(type!){sample,_ in
            complete(sample)
        }
    }
    
//    获取户外运动数据
    class func readWorkoutWithType(activityType:HKWorkoutActivityType,completion:([WorkOut]?)->Void){
        queryWithType(HKSampleType.workoutType()) { (objects,error) -> Void in
            if let results = objects as? [HKWorkout]{
                let workouts = results.map(WorkOut.init)
                completion(workouts)
            }
        }
    }
    
//    将数据保存到healthKit
    
//    保存户外运动数据模型
    class func saveWorkout(workout:WorkOut,complete:BoolResultClosure){
        let work = workout.workout
        saveObjects([work]){isSuccess,err in
            if isSuccess{
                complete(success:isSuccess, error:nil)
            }else{
                complete(success:isSuccess, error:err)
                print(err)
            }
        }
    }
    
//    保存身体健康数据
    class func saveHealthIfo(ifo:HealthInfo,complete:BoolResultClosure){
        if let weight = ifo.weightSample,
            let height = ifo.heightSample{
        self.saveObjects([weight,height],complete: complete)
        }
    }
}

extension HealthTool{
    private class func addWorkOutSample(work:HKWorkout){
        let distanceSample = HKQuantitySample(type: HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!, quantity:work.totalDistance!, startDate: work.startDate, endDate:work.endDate)
        let caloriesSample = HKQuantitySample(type: HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!, quantity: work.totalEnergyBurned!, startDate: work.startDate, endDate: work.endDate)
        self.healthStrore.addSamples([distanceSample,caloriesSample], toWorkout:work, completion: { (success, error ) -> Void in
            
        })
    }
    
    private class func saveObjects(obj:[HKObject],complete:BoolResultClosure){
        healthStrore.saveObjects(obj){
            complete(success: $0,error: $1)
        }
    }
    
//    private class func getHeight(complete:DoubleResultClosure){
//        let sampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
//        readMostRecentSample(sampleType!) {sample,err in
//            if let height = (sample as? HKQuantitySample)?.quantity.doubleValueForUnit(HKUnit.meterUnit()){
//                complete(height)
//            }else{
//                complete(nil)
//            }
//        }
//    }
//    
//    private class func getWeight(complete:DoubleResultClosure){
//        let sampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
//        readMostRecentSample(sampleType!) {sample,err in
//            if let result = (sample as? HKQuantitySample)?.quantity.doubleValueForUnit(HKUnit.gramUnit()){
//                let weight = result / 1000.0
//                complete(weight)
//            }else{
//                complete(nil)
//            }
//        }
//    }
    
    private class func readMostRecentSample(sampleType:HKSampleType,complete:(HKSample?,NSError!)->Void){
        queryWithType(sampleType) { (object,err) in
            let mostRecentSample = object.first as? HKQuantitySample
            complete(mostRecentSample,err)
        }
    }
    
    private  class func queryWithType(type:HKSampleType,completion:([AnyObject]!, NSError!) -> Void){
        let predicate:NSPredicate = {
            switch type{
            case HKSampleType.workoutType() :
                let predicate =  HKQuery.predicateForWorkoutsWithWorkoutActivityType(HKWorkoutActivityType.Running)
                return predicate
            default :
                let past = NSDate.distantPast()
                let now = NSDate()
                let mostRecentPredicate = HKQuery.predicateForSamplesWithStartDate(past, endDate:now,options: .None)
                return mostRecentPredicate
            }
        }()
        querywithType(type, predicate:predicate, completion:completion)
    }
    
    private class func querywithType(
        type:HKSampleType,
        predicate:NSPredicate,
        sortDescriptor:[NSSortDescriptor] = [NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)],
        completion:([AnyObject]!, NSError!) -> Void)
    {
        let sampleQuery = HKSampleQuery(sampleType: HKWorkoutType.workoutType(), predicate: predicate, limit: 0, sortDescriptors:sortDescriptor)
            { _,results,error in
                completion(results,error)
        }
        self.healthStrore.executeQuery(sampleQuery)
    }
    
}


