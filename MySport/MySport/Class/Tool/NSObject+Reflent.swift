//
//  NSObject+Reflent.swift
//  MySport
//
//  Created by lanou3g on 16/2/27.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit
// 获取项目的命名空间
public let namespace = NSBundle.mainBundle().infoDictionary!["CFBundleExecutable"] as! String


extension NSObject{
    var mirror:Mirror{
        return Mirror(reflecting:self)
    }
    
    func className()->String?{
        let claz = mirror.subjectType as! AnyClass
        let name:String = NSStringFromClass(claz)
        return name.removeNamespace()
    }
    
    func propertyList()->[NSObject:AnyObject]?{
        var dict:[NSObject:AnyObject]! = [NSObject:AnyObject]()
        for (lable,value) in mirror.children{
            if let obj = value as? NSObject{
                if !obj.isFundationObject() {
                   obj.propertyList()
                    dict[lable!] = obj
                    continue
                }
            }
            dict[lable!] = (value as! AnyObject)
        }
        return dict
    }
    
    func keysTuple()->(ints:[String]?,doubles:[String]?,texts:[String]?){
        var ints = [String]()
        var doubles = [String]()
        var texts = [String]()
        for (key,obj) in self.propertyList()!{
            switch obj{
            case is Int:
                ints.append(key as! String)
            case is Double :
                doubles.append(key as! String)
            case is String :
                texts.append(key as! String)
            default : break
            }
        }
        return ((ints.count>0 ? ints:nil),(doubles.count>0 ? ints:nil),(ints.count>0 ? texts:nil))
    }
    
    private func displayStyle(instance:Any) -> Mirror.DisplayStyle?{
        return Mirror(reflecting:instance).displayStyle
    }
    
    private class func isClass()->Bool{
        if let obj = self as? NSObject{
            return !obj.isFundationObject()
        }else{
            return false
        }
    }
    
    private func  isFundationObject()->Bool{
        if self.fitClassArray([NSString.self,NSValue.self]){
            return true
        }
        return false
    }
    
    private func fitClassArray(clazzs:[AnyClass])->Bool{
        for clazz in clazzs{
            if self .isKindOfClass(clazz){
                return true
            }
        }
        return false
    }
}


extension String{
    func removeNamespace()->String{
        return self.stringByReplacingOccurrencesOfString(namespace + ".", withString:"")
    }
    
    func addNamespace()->String{
        if self.hasPrefix(namespace) { return self }
        return namespace + "." + self
    }
    
    func object()->AnyObject?{
        if let clazz = (NSClassFromString(self) as? NSObject.Type){
            return clazz.init()
        }
        let name = self.addNamespace()
        if let clazz = (NSClassFromString(name) as? NSObject.Type) {
            return clazz.init()
        }
        return nil
    }
}

