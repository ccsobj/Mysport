//
//  LoginTool.swift
//  MySport
//
//  Created by lanou3g on 16/2/27.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

class LoginTool: NSObject {
    
    
    class func loginWithName(userName:String,uid:String,token:String,iconurl:String){
        
    }
    
    class func signUpWithname(name:String,password:String,complete:(Bool)->()){
        LeancloudManage.signUpWithName(name, password: password) {
                complete($0)
        }
    }

    //MARk 通过用户名密码注册
    class func signUpWithUser(user:User,complete:BoolResultClosure) {
        LeancloudManage.signUpWithUser(user, complete:complete)
    }
    
    //    请求短信注册验证码，验证码有效期10分钟
    class func requestMobilePhoneVerify(phonNumber:String,complete:BoolResultClosure){
        LeancloudManage.requestMobilePhoneVerify(phonNumber){complete(success:$0, error:$1)}
    }
    
    //    验证短信验证码是否正确
    class func verifyMobilePhone(verifyNumber:String,complete:BoolResultClosure){
        LeancloudManage.verifyMobilePhone(verifyNumber){complete(success:$0, error:$1)}
    }
    
    //    通过用户名密码登陆
    class func login(userName:String,password:String,complete:UserResultClosure){
        LeancloudManage.login(userName, password:password, complete:complete)
    }
    
    //    用手机快速登陆,获取验证码
    class func requestSmsCodeWithPhoneNumber(phoneNumber:String,complete:BoolResultClosure){
        LeancloudManage.requestSmsCodeWithPhoneNumber(phoneNumber){complete(success:$0, error:$1)}
    }
    //    手机快速登陆
    class func signUpOrLoginWithPhoneNumber(phoneNumber:String,smsCode:String,complete:UserResultClosure) {
        LeancloudManage.signUpOrLoginWithPhoneNumber(phoneNumber, smsCode:smsCode, copmplete:complete)
    }
}
