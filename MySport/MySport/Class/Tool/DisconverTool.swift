//
//  DisconverTool.swift
//  MySport
//
//  Created by lanou3g on 16/2/29.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit




class DisconverTool: NSObject {
    
    typealias CommunityResultClosure = (CommunityResult?)->()
    typealias MessageResultClosue = (MessageResult?)->()
    typealias ActybityResultClosure = (ActivityResult?)->()
    
    
    class func getComminityWithParam(
        param:ComunityParam = ComunityParam(),
        complete:CommunityResultClosure){
        
        if let dataArray = DataBaseTool.shareInstance.selectCommunity(){
            let result = CommunityResult.init()
            result.modleArray = dataArray
            complete(result)
            return
        }
        
        HttpTool.getWithParam(param){
            if let arr = $0{
                let result = CommunityResult(dictionaryArray:arr)
            DataBaseTool.shareInstance.saveCommunity(result.modleArray!)
                complete(result)
            }else{
                complete(nil)
            }
        }
    }
    //解析数据
    class func getMessage(complete:(MessageResult)->()){
        getMessageWithParam(){
            if let result = $0 {
        
                complete(result)
            }
        }
    }
    //解析数据
    class func getMessageWithParam(
        param:MessageParam = MessageParam(),
        complete:MessageResultClosue){
        
        if let dataArray = DataBaseTool.shareInstance.selectMessge(){
                let result = MessageResult.init()
                result.modleArray = dataArray
                
                complete(result)
                return
        }
        HttpTool.getWithParam(param){
            if let arr = $0{
                let result = MessageResult(dictionaryArray:arr)
                DataBaseTool.shareInstance.saveMessage(result.modleArray)
                complete(result)
            }else{
                complete(nil)
            }
        }
    }
    
    class func getActivityWithParam(
        param:ActivityParm = ActivityParm(),
        complete:ActybityResultClosure){
        
        if let dataArray = DataBaseTool.shareInstance.selectActivity(){
                let result = ActivityResult.init()
                result.modleArray = dataArray
                complete(result)
                return
        }
            
        HttpTool.getWithParam(param){
            if let arr = $0{
                let result = ActivityResult(dictionaryArray:arr)
            DataBaseTool.shareInstance.saveActivity(result.modleArray)
                complete(result)
            }else{
                complete(nil)
            }
        }
    }
}
