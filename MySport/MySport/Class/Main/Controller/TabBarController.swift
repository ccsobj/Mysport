//
//  TabBarController.swift
//  MySport
//
//  Created  on 16/2/25.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

import SnapKit
import SDWebImage

// 遵循该协议，表示可以转化为UIImage
protocol ImageConvertible{
    var image:UIImage?{ get }
}
extension UIImage:ImageConvertible{
    var image:UIImage?{
        return self
    }
}
extension String:ImageConvertible{
    var image:UIImage?{
        return UIImage(named:self)
    }
}

class TabBarController: UITabBarController,TabBarDelegate {

//   设置TabBar所需要的信息模型，包含子控制器、tabarItem的标题、图片等信息
    struct ItemMsg {
        let viewController:UIViewController
        let title:String
        let image:ImageConvertible
        let selectedImage:ImageConvertible
        var navController:UINavigationController{
            viewController.tabBarItem.title = title
            viewController.tabBarItem.image = image.image
            viewController.tabBarItem.selectedImage = selectedImage.image
            viewController.view.backgroundColor = UIColor.whiteColor()
            let nav = UINavigationController(rootViewController:viewController)
            return nav
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        UINavigationBar.appearance().barStyle = UIBarStyle.Default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        将tabBar换为自定义的
        let tabBar = TabBar()
        tabBar.delegate = self
        tabBar.clickDelegate = self
        self.setValue(tabBar, forKey:"tabBar")
//        设置tabBar上的item
        self.setupWithItemMsg([
        ItemMsg(viewController:HomeTableViewController(), title:"首页", image:"m_tab_main_not_set", selectedImage:"m_tab_main_set"),
        ItemMsg(viewController:DisconverViewController(), title:"发现", image:"tabicon_discovery_notsel", selectedImage:"tabicon_discovery_sel"),
        ItemMsg(viewController:AchieveViewController(), title:"成就", image:"tabicon_rank_notsel", selectedImage:"tabicon_rank_sel"),
        ItemMsg(viewController:ProfileViewController(), title:"我的", image:"tabicon_more_notsel", selectedImage:"tabicon_more_sel")
            ])
    }
    
    func tabBarDidClickMidBtn(tabBar: UITabBar) {
        
        let OutsideNC = UINavigationController(rootViewController: OutsideViewController())
        UINavigationBar.appearance().barStyle = UIBarStyle.Black
        self.presentViewController(OutsideNC, animated:true){
            
        }
    }
}

extension TabBarController{
    //／ 通过itemMsg模型，设置tabBar
    private func setupWithItemMsg(itemMsg:[ItemMsg]){
        
        for item in itemMsg{
            self.addChildViewController(item.navController)
        }
    }
}

