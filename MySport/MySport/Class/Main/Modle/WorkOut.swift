//
//  WorkOut.swift
//  MySport
//
//  Created by lanou3g on 16/3/4.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

import HealthKit


//女: BMR = 655 + ( 9.6 x 体重kg ) + ( 1.8 x 身高cm ) - ( 4.7 x 年龄years )
//
//男: BMR = 66 + ( 13.7 x 体重kg ) + ( 5 x 身高cm ) - ( 6.8 x 年龄years )

//久坐不动的（一点点运动或者没有运动）：卡路里 =  BMR × 1.2
//少量运动（每周1-3天轻量运动）：卡路里 = BMR × 1.375
//中等运动量（每周3-5天中等程度运动）：卡路里 =  BMR × 1.55
//高运动量（每周6-7天高强度运动）：卡路里 = BMR × 1.725
//超强度运动：卡路里 =  BMR × 1.9

//田径：每半小时可消耗热量450卡。它可使人体全身得到锻炼。
//
//自行车：每半小时消耗热量330卡。对心肺、腿十分有利。
//
//慢跑：每半小时消耗热量300卡。有益于心肺和血液循环。跑的路程越长，消耗的热量越大。
//
//散步：每半小时消耗热量75卡。对心肺功能的增强有益，它能改善血液循环，活动关节和有助于减肥。
//
//跳绳：每半小时消耗热量400卡。这是一项健美运动，对心肺系统等各种脏器、协调性、姿态、减肥等都有相当大的帮助。
//
//乒乓球：每半小时消耗热量180卡。属全身运动，有益于心肺，可锻炼重心的移动和协调性。
//
//排球：每半小时消耗热量175卡。主要增强灵活性、弹跳力和体力，有益于心肺。

//四、热量的单位换算
//
//　　千卡 Kilocalorie， 千焦耳
//
//　　1 千卡 = 4.184 千焦耳
//
//　　1 千卡： 是能使出1毫升水上升摄氏1度的热量。
//
//由于相对我们日常摄取的热量，“卡路里”这一单位的量度还是太小。现时营养学普遍采用“千卡”（又称“大卡”）为单位。1千卡等于1000卡路里，约4186焦耳。在非正式场合以及非正式书面记录中，往往将“千卡”、“大卡”省略为“卡”，读者应根据常识予以注意识别。

@objc class WorkOut: NSObject {

//  活动的类型，包括跑步，散步、骑行等
    var activityType : HKWorkoutActivityType
//    户外运动的起始时间
    var startDate : NSDate
//    户外运到的结束时间
    var endDate : NSDate
//    户外运动所经历总路径长度 单位米
    var distance : Double?
//    消耗的卡洛里  单位 kcal
    var calories : Double?
//    计算属性，将距离转化为hk质量对象
    var distanceQuantity : HKQuantity?{
        set(disq){
            distance = disq?.doubleValueForUnit(HKUnit.meterUnit())
        }
        get{
            if distance == nil {return nil}
            let quantity = HKQuantity(unit:HKUnit.meterUnit(), doubleValue:distance!)
            return quantity
        }
    }
//    计算属性，将卡洛里转化为hk质量对象
    var caloriesQuantity : HKQuantity?{
        set(caloriseQuatity){
            calories = caloriesQuantity?.doubleValueForUnit(HKUnit.kilocalorieUnit())
        }
        get{
         if calories == nil { return nil }
          let quantity = HKQuantity(unit:HKUnit.kilocalorieUnit(), doubleValue:calories!)
          return quantity
        }
    }
//    计算属性，计算活动总时长
    var duration : Double{
        return endDate.timeIntervalSinceDate(startDate)
    }
//    计算属性将所有属性，汇总为hk对象
    var workout : HKWorkout{
        set(workout){
            startDate = workout.startDate
            endDate = workout.endDate
            caloriesQuantity = workout.totalEnergyBurned
            distanceQuantity = workout.totalDistance
        }
        get{
         let workout = HKWorkout(activityType: HKWorkoutActivityType.Running, startDate:startDate, endDate: endDate, duration: self.duration , totalEnergyBurned: caloriesQuantity, totalDistance: distanceQuantity, metadata: nil)
          return workout
        }
    }
    
//    初始化方法
    init(type:HKWorkoutActivityType, start:NSDate, end:NSDate, totalCalories:Double, totalDistance:Double) {
        activityType = type
        startDate = start
        endDate = end
        calories = totalCalories
        distance = totalDistance
        super.init()
    }
    init(workout:HKWorkout) {
        startDate = workout.startDate
        endDate = workout.endDate
        calories = workout.totalEnergyBurned?.doubleValueForUnit(HKUnit.kilocalorieUnit())
        distance = workout.totalDistance?.doubleValueForUnit(HKUnit.meterUnit())
        activityType = workout.workoutActivityType
        super.init()
    }
}
