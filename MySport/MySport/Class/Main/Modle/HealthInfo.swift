//
//  Health.swift
//  MySport
//
//  Created by lanou3g on 16/2/27.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

import HealthKit

@objc class HealthInfo: NSObject {
//    体重 单位kg
    var weight:Double?{
        get{
            let value =  (self.weightSample as? HKQuantitySample)?.quantity.doubleValueForUnit(HKUnit.gramUnit())
            if value == nil { return nil}
             return value! / 1000.0
        }
        set{
            if weight == nil { return }
            let quantity = HKQuantity(unit:HKUnit.gramUnit(), doubleValue:weight! * 100)
            let sample = HKQuantitySample(type:HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!, quantity:quantity, startDate:NSDate(), endDate:NSDate())
            weightSample = sample
        }
    }
//    身高 单位米
    var height:Double?{
        get{
            let value =  (self.heightSample as? HKQuantitySample)?.quantity.doubleValueForUnit(HKUnit.meterUnit())
            if value == nil { return nil}
            return value!
        }
        set{
            if height == nil { return }
            let quantity = HKQuantity(unit:HKUnit.gramUnit(), doubleValue:height! )
            let sample = HKQuantitySample(type:HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!, quantity:quantity, startDate:NSDate(), endDate:NSDate())
            heightSample = sample
        }
    }
    
    var vitalCapacity:Double?{
        get{
            let value =  (self.vitalCapacitySample as? HKQuantitySample)?.quantity.doubleValueForUnit(HKUnit.literUnit())
            if value == nil { return nil}
            return value! * 1000
        }
        set{
            if vitalCapacity == nil { return }
            let quantity = HKQuantity(unit:HKUnit.literUnit(), doubleValue:vitalCapacity! / 100)
            let sample = HKQuantitySample(type:HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!, quantity:quantity, startDate:NSDate(), endDate:NSDate())
            vitalCapacitySample = sample
        }
    }
    
    
//   体质指数 IBM值，由身高体重计算得到
    var BMI : Double?{
        if (weight == nil) || (height == nil) { return nil}
        return self.weight! / (self.height! * self.height!)
    }
    
//    血型
    var bloodType:HKBloodType?
//    性别
    var sex:HKBiologicalSex?
//    出身日期
    dynamic var birthDay:NSDate?

    var weightSample : HKSample?
    
    var heightSample : HKSample?
    
    var BMISample : HKSample?
    
    var vitalCapacitySample : HKSample?
    
}
