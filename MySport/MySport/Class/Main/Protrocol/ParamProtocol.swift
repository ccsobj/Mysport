//
//  ParamProtocol.swift
//  WangYiNews
//
//  Created by lanou3g on 16/1/18.
//  Copyright © 2016年 lanou3g. All rights reserved.
//

import UIKit

@objc protocol ParamProtocol {
    var URLString:String {get}
    var paramDic:[String:String]? {get}
    
//    同过数据请求返回json（大字典或数组）后，将其转换为包含（可转化为模型的）字典的数组
    func dictionaryArray(json:AnyObject) ->[[String:AnyObject]]?
    
}
