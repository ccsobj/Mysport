//
//  PushableProtrocol.swift
//  MySport
//
//  Created by lanou3g on 16/3/2.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

import UIKit

protocol PushableProtocol :class {
    weak var navVC : UINavigationController? { set get }
//    var modleArray : CommunityResult? { get }
    var tableView: UITableView! { get }
    var refreshControl: UIRefreshControl? { get }
    func pushViewController(vc:UIViewController)
    
}

extension PushableProtocol  where Self :UIViewController{
    
    func pushViewController(vc:UIViewController){
        self.navVC?.pushViewController(vc, animated: true)
    }
    
}




