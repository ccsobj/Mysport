//
//  ResultProtocol.swift
//  WangYiNews
//
//  Created by lanou3g on 16/1/18.
//  Copyright © 2016年 lanou3g. All rights reserved.
//

import UIKit

protocol ResultProtocol:class{
    typealias ModleClass
    var modleArray:[ModleClass]!{get set}
    var count : Int { get }
    init(dictionaryArray:[[String:AnyObject]])
    subscript(index:Int)->ModleClass?{get}
}


extension ResultProtocol  where Self : NSObject {
   var count : Int {
        if let count = self.modleArray?.count{
            return count
        }else{
            return 0
        }
    }
    
    subscript(index:Int)->ModleClass?{
        get{
        return self.modleArray[safeIndex(index)]
        }
        set{
            if newValue == nil { return }
            self.modleArray[safeIndex(index)] = newValue!
        }
    }
    
    private func safeIndex(index:Int)->Int{
        if index < 0{
        return self.modleArray.count - 1
        }else if index >= self.modleArray.count{
            return  0
        }else{
            return index
        }
    }
}