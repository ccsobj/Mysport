//
//  AchieveViewController.m
//  MySport
//
//  Created by lanou3g on 16/2/26.
//  Copyright © 2016年 ccsobj. All rights reserved.
//

#import "AchieveViewController.h"
#import "AchieveView.h"
#import "AchieveViewTableViewCell.h"
@interface AchieveViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)AchieveView * achieveview;
@property (nonatomic ,strong)NSMutableArray *arr;
@end

@implementation AchieveViewController
-(void)loadView{
    self.achieveview = [[AchieveView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.view = self.achieveview;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //设置代理
    self.achieveview.tableview.delegate = self;
    self.achieveview.tableview.dataSource = self;
    //上方空白处

    

    [self addviews];
    
}
-(void)addviews{
    self.arr = [[NSMutableArray alloc]init];
    for (int i  = 1; i<20; i++) {
        [self.arr addObject:[NSNumber numberWithInt:i]];
    }

}

//分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arr.count;
}
//cell高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}
//头视图 为0.1
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    AchieveViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    AchieveViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    //判断是cell是否存在，不存在的话就创建一个
    if (cell == nil) {
        cell = [[AchieveViewTableViewCell alloc] initWithStyle:(UITableViewCellStyleSubtitle) reuseIdentifier:@"cellID"];
    }

//前三名变色
    if (indexPath.row ==2 ) {
        cell.rankinglabel.textColor = [UIColor orangeColor];
        cell.numberlabel.textColor = [UIColor orangeColor];
        cell.namelabel.textColor = [UIColor orangeColor];
        cell.sexlabel.textColor = [UIColor orangeColor];
    }else{
        cell.rankinglabel.textColor = [UIColor blackColor];
        cell.numberlabel.textColor = [UIColor blackColor];
        cell.namelabel.textColor = [UIColor blackColor];
        cell.sexlabel.textColor = [UIColor blackColor];
    }
    if (indexPath.row ==1 ) {
        cell.rankinglabel.textColor = [UIColor cyanColor];
        cell.numberlabel.textColor = [UIColor cyanColor];
        cell.namelabel.textColor = [UIColor cyanColor];
        cell.sexlabel.textColor = [UIColor cyanColor];
    }

    if (indexPath.row ==0 ) {
        cell.rankinglabel.textColor = [UIColor redColor];
        cell.numberlabel.textColor = [UIColor redColor];
        cell.namelabel.textColor = [UIColor redColor];
        cell.sexlabel.textColor = [UIColor redColor];
    }
    //前三名为女
    if (indexPath.row < 3) {
        cell.sexlabel.text = @"♀";
    }else{
        cell.sexlabel.text = @"♂";
        
    }
    
    
    cell.rankinglabel.text = [NSString stringWithFormat:@"%@",self.arr[indexPath.row]];
    cell.numberlabel.text = @"1111";
    cell.namelabel.text = @"姓名";
    cell.image.image = [UIImage imageNamed:@"1.jpg"];
    return cell;
}









- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
